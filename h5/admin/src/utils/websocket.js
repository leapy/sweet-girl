import Cookies from 'js-cookie'
const defaultSettings = require('../../src/settings.js');

const TokenKey = 'Admin-Token';

const ws = new WebSocket(defaultSettings.websocketHost);

/**
 * 当打开 WebSocket时 登录
 */
ws.onopen = function (){
  let data = {
    'event': 'login',
    'data': {
      'token': Cookies.get(TokenKey)
    }
  }
}

export default ws;
