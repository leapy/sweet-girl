import request from '@/utils/request'

/**
 * 权限列表
 * @returns {AxiosPromise}
 */
export function menuList(params) {
  return request({
    url: `/menu/list`,
    method: 'get',
    params: params
  })
}

/**
 * 改变单个字段
 * @returns {AxiosPromise}
 */
export function changeField(data) {
  return request({
    url: `/menu/field`,
    method: 'put',
    data: data
  })
}

/**
 * 添加菜单
 * @returns {AxiosPromise}
 */
export function menuSubmit(data) {
  return request({
    url: `/menu/submit`,
    method: 'post',
    data: data
  })
}

/**
 * 添加菜单
 * @returns {AxiosPromise}
 */
export function menuDel(data) {
  return request({
    url: `/menu/del`,
    method: 'delete',
    data:data
  })
}

/**
 * 菜单选项
 * @returns {AxiosPromise}
 */
export function menuOption() {
  return request({
    url: `/menu/menu/option`,
    method: 'get',
  })
}

/**
 * 角色列表
 * @returns {AxiosPromise}
 */
export function roleList(params) {
  return request({
    url: '/role/list',
    method: 'get',
    params:params
  })
}
