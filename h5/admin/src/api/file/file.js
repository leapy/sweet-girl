import request from "@/utils/request";

/**
 * option
 * @returns {AxiosPromise}
 */
export function fileList(params) {
  return request({
    url: `/file/file/lst`,
    method: 'get',
    params:params
  })
}
