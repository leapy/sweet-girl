import request from "@/utils/request";

/**
 * 保存
 * @returns {AxiosPromise}
 */
export function menuSave(data) {
  return request({
    url: `/file/menu/save`,
    method: 'post',
    data: data
  })
}

/**
 * 删除
 * @returns {AxiosPromise}
 */
export function menuDel(data) {
  return request({
    url: `/file/menu/del`,
    method: 'delete',
    data:data
  })
}

/**
 * option
 * @returns {AxiosPromise}
 */
export function menuOption() {
  return request({
    url: `/file/menu/option`,
    method: 'get',
  })
}

/**
 * option
 * @returns {AxiosPromise}
 */
export function menuLst() {
  return request({
    url: `/file/menu/lst`,
    method: 'get',
  })
}
