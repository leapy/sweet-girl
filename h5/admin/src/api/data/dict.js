import request from '@/utils/request'

/**
 * 列表
 * @returns {AxiosPromise}
 */
export function lst(params) {
  return request({
    url: `/dict/list`,
    method: 'get',
    params: params
  })
}

/**
 * 改变单个字段
 * @returns {AxiosPromise}
 */
export function changeField(data) {
  return request({
    url: `/dict/field`,
    method: 'put',
    data: data
  })
}

/**
 * 保存
 * @returns {AxiosPromise}
 */
export function save(data) {
  return request({
    url: `/dict/save`,
    method: 'post',
    data: data
  })
}

/**
 * 删除
 * @returns {AxiosPromise}
 */
export function del(data) {
  return request({
    url: `/dict/del`,
    method: 'delete',
    data:data
  })
}

/**
 * 启用
 * @returns {AxiosPromise}
 */
export function enabled(data) {
  return request({
    url: `/dict/enabled`,
    method: 'put',
    data: data
  })
}

/**
 * 禁用
 * @returns {AxiosPromise}
 */
export function disEnabled(data) {
  return request({
    url: `/dict/disabled`,
    method: 'put',
    data: data
  })
}

/**
 * options
 * @returns {AxiosPromise}
 */
export function options() {
  return request({
    url: `/dict_tab/options`,
    method: 'get',
  })
}

