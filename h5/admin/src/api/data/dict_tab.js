import request from '@/utils/request'

/**
 * 列表
 * @returns {AxiosPromise}
 */
export function lst(params) {
  return request({
    url: `/dict_tab/list`,
    method: 'get',
    params: params
  })
}

/**
 * 改变单个字段
 * @returns {AxiosPromise}
 */
export function changeField(data) {
  return request({
    url: `/dict_tab/field`,
    method: 'put',
    data: data
  })
}

/**
 * 保存
 * @returns {AxiosPromise}
 */
export function save(data) {
  return request({
    url: `/dict_tab/save`,
    method: 'post',
    data: data
  })
}

/**
 * 删除
 * @returns {AxiosPromise}
 */
export function del(data) {
  return request({
    url: `/dict_tab/del`,
    method: 'delete',
    data:data
  })
}

/**
 * 启用
 * @returns {AxiosPromise}
 */
export function enabled(data) {
  return request({
    url: `/dict_tab/enabled`,
    method: 'put',
    data: data
  })
}

/**
 * 禁用
 * @returns {AxiosPromise}
 */
export function disEnabled(data) {
  return request({
    url: `/dict_tab/disabled`,
    method: 'put',
    data: data
  })
}
