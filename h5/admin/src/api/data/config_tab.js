import request from '@/utils/request'

/**
 * 地址列表
 * @returns {AxiosPromise}
 */
export function lst(params) {
  return request({
    url: `/system/config/tab/list`,
    method: 'get',
    params: params
  })
}

/**
 * 改变单个字段
 * @returns {AxiosPromise}
 */
export function changeField(data) {
  return request({
    url: `/system/config/tab/field`,
    method: 'put',
    data: data
  })
}

/**
 * 添加地区
 * @returns {AxiosPromise}
 */
export function save(data) {
  return request({
    url: `/system/config/tab/save`,
    method: 'post',
    data: data
  })
}

/**
 * 删除
 * @returns {AxiosPromise}
 */
export function del(data) {
  return request({
    url: `/system/config/tab/del`,
    method: 'delete',
    data:data
  })
}

/**
 * 启用
 * @returns {AxiosPromise}
 */
export function enabled(data) {
  return request({
    url: `/system/config/tab/enabled`,
    method: 'put',
    data: data
  })
}

/**
 * 禁用
 * @returns {AxiosPromise}
 */
export function disEnabled(data) {
  return request({
    url: `/system/config/tab/disabled`,
    method: 'put',
    data: data
  })
}
