import request from '@/utils/request'

/**
 * 地址列表
 * @returns {AxiosPromise}
 */
export function lst(params) {
  return request({
    url: `/address/list`,
    method: 'get',
    params: params
  })
}

/**
 * 改变单个字段
 * @returns {AxiosPromise}
 */
export function changeField(data) {
  return request({
    url: `/address/field`,
    method: 'put',
    data: data
  })
}

/**
 * 添加地区
 * @returns {AxiosPromise}
 */
export function save(data) {
  return request({
    url: `/address/save`,
    method: 'post',
    data: data
  })
}

/**
 * 删除
 * @returns {AxiosPromise}
 */
export function del(data) {
  return request({
    url: `/address/del`,
    method: 'delete',
    data:data
  })
}
