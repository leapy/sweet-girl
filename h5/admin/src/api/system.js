import request from '@/utils/request'

/**
 * 获取所有配置项
 * @returns {AxiosPromise}
 */
export function items(params) {
  return request({
    url: `/system/config/items`,
    method: 'get',
    params: params
  })
}

/**
 * 获取所有配置项
 * @returns {AxiosPromise}
 */
export function save(data) {
  return request({
    url: `/system/config/saveAll`,
    method: 'post',
    data: data
  })
}

/**
 * 发送短信测试
 * @returns {AxiosPromise}
 */
export function sendSms(params) {
  return request({
    url: `/system/config/sendSms`,
    method: 'get',
    params: params
  })
}

/**
 * 发送邮件测试
 * @returns {AxiosPromise}
 */
export function sendEmail(params) {
  return request({
    url: `/system/config/sendEmail`,
    method: 'get',
    params: params
  })
}

/**
 * 邮件模板
 * @returns {AxiosPromise}
 */
export function emailTemp(params) {
  return request({
    url: `/system/config/emailTemp`,
    method: 'get',
    params: params
  })
}

/**
 * 保存邮件模板
 * @returns {AxiosPromise}
 */
export function saveEmailTemp(data) {
  return request({
    url: `/system/config/saveEmailTemp`,
    method: 'post',
    data: data
  })
}

/**
 * 删除
 * @returns {AxiosPromise}
 */
export function delImage(data) {
  return request({
    url: `/upload/image/del`,
    method: 'delete',
    data: data
  })
}

