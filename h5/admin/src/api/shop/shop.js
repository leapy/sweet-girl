import request from '@/utils/request'

/**
 * 登录
 * @param params
 * @returns {AxiosPromise}
 */
export function lst(params) {
  return request({
    url: '/shop/lst',
    method: 'get',
    params
  })
}

/**
 * 改变单个字段
 * @returns {AxiosPromise}
 */
export function changeField(data) {
  return request({
    url: `/shop/field`,
    method: 'put',
    data: data
  })
}

/**
 * 删除
 * @returns {AxiosPromise}
 */
export function del(data) {
  return request({
    url: `/shop/del`,
    method: 'delete',
    data:data
  })
}

/**
 * 保存
 * @returns {AxiosPromise}
 */
export function save(data) {
  return request({
    url: `/shop/save`,
    method: 'post',
    data: data
  })
}
