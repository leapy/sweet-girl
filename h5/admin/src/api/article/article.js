import request from '@/utils/request'

/**
 * 地址列表
 * @returns {AxiosPromise}
 */
export function lst(params) {
  return request({
    url: `/article/article/list`,
    method: 'get',
    params: params
  })
}

/**
 * 改变单个字段
 * @returns {AxiosPromise}
 */
export function changeField(data) {
  return request({
    url: `/article/article/field`,
    method: 'put',
    data: data
  })
}

/**
 * 添加
 * @returns {AxiosPromise}
 */
export function save(data) {
  return request({
    url: `/article/article/save`,
    method: 'post',
    data: data
  })
}

/**
 * 删除
 * @returns {AxiosPromise}
 */
export function del(data) {
  return request({
    url: `/article/article/del`,
    method: 'delete',
    data:data
  })
}

/**
 * 启用
 * @returns {AxiosPromise}
 */
export function enabled(data) {
  return request({
    url: `/article/article/enabled`,
    method: 'put',
    data: data
  })
}

/**
 * 禁用
 * @returns {AxiosPromise}
 */
export function disEnabled(data) {
  return request({
    url: `/article/article/disabled`,
    method: 'put',
    data: data
  })
}

/**
 * 配置分类选项
 * @returns {AxiosPromise}
 */
export function options() {
  return request({
    url: `/article/category/options`,
    method: 'get',
  })
}
