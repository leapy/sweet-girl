import request from '@/utils/request'

/**
 * 登录
 * @param data
 * @returns {AxiosPromise}
 */
export function login(data) {
  return request({
    url: '/login',
    method: 'post',
    data
  })
}

/**
 * 用户信息
 * @returns {AxiosPromise}
 */
export function getInfo() {
  return request({
    url: '/info',
    method: 'post',
  })
}

/**
 * 退出
 * @returns {AxiosPromise}
 */
export function logout() {
  return request({
    url: '/logout',
    method: 'get'
  })
}

/**
 * 用户列表
 * @returns {AxiosPromise}
 */
export function userList(params) {
  return request({
    url: '/user/list',
    method: 'get',
    params:params
  })
}

/**
 * 改变单个字段
 * @returns {AxiosPromise}
 */
export function changeField(data) {
  return request({
    url: '/user/field',
    method: 'put',
    data:data
  })
}

/**
 * 保存
 * @returns {AxiosPromise}
 */
export function save(data) {
  return request({
    url: `/user/save`,
    method: 'post',
    data: data
  })
}

/**
 * 删除
 * @param data
 * @returns {AxiosPromise}
 */
export function del(data) {
  return request({
    url: `/user/del`,
    method: 'delete',
    data:data
  })
}

/**
 * 菜单选项
 * @returns {AxiosPromise}
 */
export function roleOption() {
  return request({
    url: `/role/role/option`,
    method: 'get',
  })
}

/**
 * 启用
 * @returns {AxiosPromise}
 */
export function enabled(data) {
  return request({
    url: `/user/enabled`,
    method: 'put',
    data: data
  })
}

/**
 * 禁用
 * @returns {AxiosPromise}
 */
export function disEnabled(data) {
  return request({
    url: `/user/disabled`,
    method: 'put',
    data: data
  })
}

/**
 * 修改密码
 * @param new_password
 * @param old_password
 * @returns {AxiosPromise}
 */
export function changePassword(data) {
  return request({
    url: `/admin/changePassword`,
    method: 'post',
    data: data
  })
}

/**
 * 实时查看自己的信息
 * @returns {AxiosPromise}
 */
export function info() {
  return request({
    url: `/base/info`,
    method: 'get',
  })
}

/**
 * 修改密码
 * @param new_password
 * @param old_password
 * @returns {AxiosPromise}
 */
export function saveInfo(data) {
  return request({
    url: `/base/info/save`,
    method: 'post',
    data: data
  })
}

/**
 * 列表
 * @returns {AxiosPromise}
 */
export function log(params) {
  return request({
    url: '/admin/log/lst',
    method: 'get',
    params: params
  })
}

/**
 * 安全信息
 * @returns {AxiosPromise}
 */
export function securityInfo(params) {
  return request({
    url: '/admin/security/info',
    method: 'get',
    params: params
  })
}

/**
 * 安全信息
 * @returns {AxiosPromise}
 */
export function modify(data) {
  return request({
    url: '/admin/modify',
    method: 'post',
    data: data
  })
}

/**
 * 验证手机号
 * @returns {AxiosPromise}
 */
export function verify(data) {
  return request({
    url: '/admin/modify/verify',
    method: 'post',
    data: data
  })
}

