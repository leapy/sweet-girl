import request from '@/utils/request'

/**
 * 权限列表
 * @returns {AxiosPromise}
 */
export function authList(params) {
  return request({
    url: `/auth/list`,
    method: 'get',
    params: params
  })
}

/**
 * 改变单个字段
 * @returns {AxiosPromise}
 */
export function changeField(data) {
  return request({
    url: `/auth/field`,
    method: 'put',
    data: data
  })
}

/**
 * 添加
 * @returns {AxiosPromise}
 */
export function authSubmit(data) {
  return request({
    url: `/auth/submit`,
    method: 'post',
    data: data
  })
}

/**
 * 删除
 * @returns {AxiosPromise}
 */
export function authDel(data) {
  return request({
    url: `/auth/del`,
    method: 'delete',
    data:data
  })
}

/**
 * 选项
 * @returns {AxiosPromise}
 */
export function authOption() {
  return request({
    url: `/auth/option`,
    method: 'get',
  })
}
