import request from '@/utils/request'

/**
 * 权限列表
 * @returns {AxiosPromise}
 */
export function roleList(params) {
  return request({
    url: `/role/list`,
    method: 'get',
    params: params
  })
}

/**
 * 改变单个字段
 * @returns {AxiosPromise}
 */
export function changeField(data) {
  return request({
    url: `/role/field`,
    method: 'put',
    data: data
  })
}

/**
 * 添加菜单
 * @returns {AxiosPromise}
 */
export function roleSubmit(data) {
  return request({
    url: `/role/submit`,
    method: 'post',
    data: data
  })
}

/**
 * 添加菜单
 * @returns {AxiosPromise}
 */
export function roleDel(data) {
  return request({
    url: `/role/del`,
    method: 'delete',
    data:data
  })
}

/**
 * 菜单选项
 * @returns {AxiosPromise}
 */
export function roleOption() {
  return request({
    url: `/role/role/option`,
    method: 'get',
  })
}

/**
 * 权限组
 * @returns {AxiosPromise}
 */
export function authOption() {
  return request({
    url: `/role/auth/option`,
    method: 'get',
  })
}

/**
 * 菜单
 * @returns {AxiosPromise}
 */
export function menuOption() {
  return request({
    url: `/role/menu/option`,
    method: 'get',
  })
}
