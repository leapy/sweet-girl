import request from '@/utils/request'

/**
 * 保存
 * @returns {AxiosPromise}
 */
export function save(data) {
  return request({
    url: `/product/cate/save`,
    method: 'post',
    data: data
  })
}

/**
 * 列表
 * @returns {AxiosPromise}
 */
export function list(params) {
  return request({
    url: `/product/cate/lst`,
    method: 'get',
    params: params
  })
}

/**
 * 改变单个字段
 * @returns {AxiosPromise}
 */
export function changeField(data) {
  return request({
    url: `/product/cate/field`,
    method: 'put',
    data: data
  })
}

/**
 * 删除
 * @returns {AxiosPromise}
 */
export function del(data) {
  return request({
    url: `/product/cate/del`,
    method: 'delete',
    data:data
  })
}

/**
 * 列表
 * @returns {AxiosPromise}
 */
export function options(params) {
  return request({
    url: `/product/cate/options`,
    method: 'get',
    params: params
  })
}

/**
 * 选项
 * @returns {AxiosPromise}
 */
export function choice(params) {
  return request({
    url: `/product/cate/choice`,
    method: 'get',
    params: params
  })
}
