import request from '@/utils/request'

/**
 * 保存
 * @returns {AxiosPromise}
 */
export function save(data) {
  return request({
    url: `/product/save`,
    method: 'post',
    data: data
  })
}

/**
 * info
 * @returns {AxiosPromise}
 */
export function info(params) {
  return request({
    url: `/product/detail`,
    method: 'get',
    params: params
  })
}

/**
 * 列表
 * @returns {AxiosPromise}
 */
export function list(params) {
  return request({
    url: `/product/list`,
    method: 'get',
    params: params
  })
}

/**
 * 列表
 * @returns {AxiosPromise}
 */
export function counts() {
  return request({
    url: `/product/counts`,
    method: 'get'
  })
}

/**
 * 改变单个字段
 * @returns {AxiosPromise}
 */
export function changeField(data) {
  return request({
    url: `/product/field`,
    method: 'put',
    data: data
  })
}

/**
 * 删除
 * @returns {AxiosPromise}
 */
export function del(data) {
  return request({
    url: `/product/del`,
    method: 'delete',
    data:data
  })
}
