<?php


use link\swoole\Handler;
use link\swoole\Parser;

return [
    'worker_num'   => 1,
    'daemonize'    => false,
    'log_file'     =>  runtime_path() . 'timer.pid',
    'task_worker_num'  => 1
];
