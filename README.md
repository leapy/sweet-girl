Sweet Girl 直播商城
===============

本项目尚在开发中，计划完成的功能有：商城、直播、拼团、秒杀、砍价、客服。  
本项目计划打通公众号，微信小程序，云闪付小程序，支付宝小程序，H5等。  
本项目特点：同一后台控制多端页面，同一手机号判断为同一用户。

## 2020.11.09
下次计划更新：商城多环境登录，购物车，个人中心，后台环境配置项

## 2020.10.29 
下次计划更新：后台多店铺管理，商品管理，图片管理，商城首页

## 运行环境

后端运行环境
> Apache或Nginx  
> PHP 7.1+  
> Mysql 5.7.26  
> Redis 5.0.5  

前端运行环境
> Node v12.10.0+  
> Npm 6.10.3+  

## 使用框架

* 后端采用ThinkPHP开源框架
* 商城前端采用Uni-app框架
* 管理后台采用Vue-Element-Ui

## 编辑器

* PhpStorm
* HBuilder

## 安装运行

下载
~~~
git clone https://gitee.com/leapy/sweet-girl.git
~~~

后端安装
~~~
composer install
~~~

管理后台安装
~~~
npm install
~~~

管理后台运行
~~~
npm run dev
~~~

管理后台打包
~~~
npm run build:prod
~~~

商城前端请在HBuilder中打包运行

## 文档

[完善中]

## 商城截图

![首页](https://asset.leapy.cn/upload/image/20201109/b08ba1260af4622670f505f62e8015d8.gif)  

（首页）

## 后台截图

![图片管理](https://asset.leapy.cn/upload/image/20201109/0e6c340dc8830a9dc9ec90becfd1a54d.png)  

（图片管理）

## 版权信息

[Sweet Girl 直播商城] 遵循MIT开源协议发布，并提供免费使用。

本项目仅供参考，学习研究使用，不得用于商业用途。

如需商业使用请联系作者。

更多细节参阅 [LICENSE](LICENSE)
