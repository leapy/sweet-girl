<?php
// 事件定义文件
return [
    'bind'      => [
    ],

    'listen'    => [
        'AppInit'  => [],
        'HttpRun'  => [],
        'HttpEnd'  => [],
        'LogLevel' => [],
        'LogWrite' => [],
        'Timer_10' => [],  // 10s定时器
        'Timer_30' => [],  // 30s定时器,
    ],

    'subscribe' => [
        \link\subscribe\Admin::class,
        \link\subscribe\Timer::class,
        \link\subscribe\User::class
    ],
];
