<?php


namespace app\index\controller;

/**
 * Class Index
 * @package app\index\controller
 */
class Index
{
    public function index()
    {
        return "欢迎访问！";
    }

    public function test()
    {
        $e = event('swoole.websocket.SendAll', $_POST);
        return json_encode($e);
    }
}
