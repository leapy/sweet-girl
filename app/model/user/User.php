<?php


namespace app\model\user;


use app\model\FrameModel;
use app\model\ToolsTrait;

/**
 * Class User
 * @package app\model\user
 */
class User extends FrameModel
{
    use ToolsTrait;

    /**
     * 添加用户
     * @param $data
     * @return int|string
     */
    public static function addUser($data)
    {
        return self::insertGetId([
            'pid' => $data['pid'],
            'group_id' => 0,
            'username' => $data['username'],
            'password' => md5($data['password']),
            'tel' => $data['tel'],
            'money' => 0,
            'integral' => 0,
            'level' => 1,
            'status' => 1,
            'add_time' => time(),
            'add_ip' => app("request")->ip(),
            'register_type' => $data['register_type'],
            'count' => 0,
            'sign_count' => 0
        ]);
    }

    /**
     * 吉祥豆增加
     * @param int $uid
     * @param int $num
     * @return User
     */
    public static function addIntegral(int $uid,int $num)
    {
        $oldValue = self::where("uid",$uid)->value("integral");
        $newValue = bcadd($oldValue,$num,0);
        return self::where("uid",$uid)->update(['integral'=>$newValue]);
    }
}
