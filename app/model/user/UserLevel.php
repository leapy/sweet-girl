<?php


namespace app\model\user;


use app\model\FrameModel;
use app\model\ToolsTrait;

/**
 * Class UserLevel
 * @package app\model\user
 */
class UserLevel extends FrameModel
{
    use ToolsTrait;
}
