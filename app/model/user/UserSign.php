<?php


namespace app\model\user;


use app\model\FrameModel;
use app\model\ToolsTrait;

/**
 * Class UserSign
 * @package app\model\user
 */
class UserSign extends FrameModel
{
    use ToolsTrait;
}
