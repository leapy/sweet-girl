<?php


namespace app\model\user;


use app\model\FrameModel;
use app\model\ToolsTrait;

/**
 * Class UserBill
 * @package app\model\user
 */
class UserBill extends FrameModel
{
    use ToolsTrait;

    /**
     * 添加账单
     * @param array $data
     * @return int|string
     */
    public static function addBill(array $data)
    {
        return self::insert([
            'uid' => $data['uid'],
            'title' => $data['title'],
            'bill_type' => isset($data['bill_type']) ? $data['bill_type'] : 0,
            'amount' => isset($data['amount']) ? $data['amount'] : 0,
            'change_type' => isset($data['remark']) ? $data['remark'] : "money",
            'result' => isset($data['result']) ? $data['result'] : "",
            'remark' => isset($data['remark']) ? $data['remark'] : "",
            'add_time' => time()
        ]);
    }
}
