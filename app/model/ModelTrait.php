<?php
declare (strict_types = 1);

namespace app\model;

/**
 * 公共模板方法
 * Trait ModelTrait
 * @package app
 */
trait ModelTrait
{
    /**
     * 获取数据总数
     * @param $model
     * @return int
     */
    public static function counts($model)
    {
        return $model->count();
    }
}
