<?php


namespace app\model;

use link\exception\ModelException;

/**
 * 工具类
 * Class ToolsTrait
 * @package app\model
 */
trait ToolsTrait
{
    /**
     * 不参与判断的字段
     * @var string[]
     */
    public static $ignore_field = ['page', 'limit', 'start_time', 'end_time'];

    /**
     * 排序字段
     * @var string
     */
    public static $rank_field = "rank desc,create_time desc";

    /**
     * 字段追加文本
     * @param $id
     * @param $field
     * @param $value
     * @param string $join
     * @return bool
     */
    public static function appendValue($id, $field, $value, $join = "")
    {
        $model = new self;
        if (empty($id) || !$id) throw new ModelException("未设置主键值");
        if (!adminId()) throw new ModelException("未登录，不允许操作");
        $value = $model->where($model->getPk(),$id)->value($field).$join.$value;
        if ($model->update([$field=>$value,'update_time'=>time(),'update_user'=>adminId()],[$model->getPk()=>$id])) return true;
        throw new ModelException("设置失败");
    }

    /**
     * 某字段是否存在
     * @param $field
     * @param $value
     * @return bool
     */
    public static function issetValue($field, $value)
    {
        $model = new self;
        return $model->where($field,$value)->find() ? true : false;
    }

    /**
     * 数据列表自动筛选查询
     * @param array $where
     * @param array $condition
     * @return array
     */
    public static function list(array $where, array $condition = []): array
    {
        $model = new self;
        foreach ($where as $key => $value)
        {
            if (!in_array($key, self::$ignore_field))
            {
                if (array_key_exists($key, $condition))
                {
                    switch ($condition[$key])
                    {
                        case "not like":
                        case "like":
                            if ($value != "") $model = $model->where($key,"like","%$value%");
                            break;
                        case "not between":
                        case "between":
                            // 时间判断
                            if (isset($where['start_time']) && isset($where['end_time']) && !empty($where['start_time']) && !empty($where['end_time']))
                                $model = $model->where($key, $condition[$key], [$where['start_time'], $where['end_time']]);
                            break;
                        case "not null":
                            if ($value != "") $model = $model->whereNotNull($key);
                            break;
                        case "null":
                            if ($value != "") $model = $model->whereNull($key);
                            break;
                        case ">time":
                        case "<time":
                        case "<=time":
                        case ">=time":
                        case ">":
                        case "<":
                        case "<>":
                        case "<=":
                        case ">=":
                        case "=":
                        default:
                            if (strlen($value) > 0) $model = $model->where($key, $condition[$key], $value);
                            break;
                    }
                }else
                {
                    if (strlen($value) > 0) $model = $model->where($key, $value);
                }
            }
        }
        // 总数
        $count = self::counts($model);
        // 分页
        if (isset($where['page']) && isset($where['limit']) && !empty($where['page']) && !empty($where['limit']))
            $model = $model->page((int)$where['page'], (int)$where['limit']);
        $model = $model->order(self::$rank_field);
        $data = $model->select();
        // 查询
        if ($data) $data = $data->toArray();
        return compact("data", "count");
    }

    /**
     * 横线
     * @param int $num
     * @return string
     */
    public static function cross(int $num=0): string
    {
        $str = "";
        if ($num == 1) $str .= "|--";
        elseif ($num > 1) for($i=0;$i<$num;$i++)
            if ($i==0) $str .= "|--";
            else $str .= "--";
        return $str." ";
    }

    /**
     * 更新或者插入
     * @param array $data
     * @return bool
     */
    public static function submit(array $data)
    {
        $model= new self;
        if (isset($data[$model->getPk()]) && $data[$model->getPk()] != "")
        {
            $data['update_user'] = adminId();
            $data['update_time'] = time();
            $res = $model::update($data,['id'=>$data[$model->getPk()]]);
        }else
        {
            $data['create_user'] = adminId();
            $data['create_time'] = time();
            $res = $model::insert($data);
        }
        return $res ? true : false;
    }
}
