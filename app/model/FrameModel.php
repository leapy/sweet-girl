<?php


namespace app\model;


use app\BaseModel;

/**
 * 基础框架 model
 * Class FrameModel
 * @package app\model
 */
class FrameModel extends BaseModel
{
    use ModelTrait;
}
