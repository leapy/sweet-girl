<?php


namespace app\model\admin;


use app\model\FrameModel;

/**
 * 操作日志
 * Class AdminLog
 * @package app\model\admin
 */
class AdminLog extends FrameModel
{
    /**
     * 保存日志
     * @param array $adminInfo
     * @param string $module
     * @param string $controller
     * @param string $action
     * @return bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function saveLog(array $adminInfo, string $module, string $controller, string $action): bool
    {
        return self::create([
            'admin_id'      => $adminInfo['id'],
            'admin_name'    => $adminInfo['name'],
            'active_name'   => AdminAuth::getLogName($module,$controller,$action),
            'module'        => $module,
            'controller'    => $controller,
            'action'        => $action,
            'ip'            => request()->ip(),
            'create_time'   => time(),
            'user_agent'     => substr(request()->server('HTTP_USER_AGENT'), 0, 255),
        ]) ? true : false;
    }

    /**
     * 操作日志列表
     * @param array $where
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function lst(array $where)
    {
        $model = new self;
        if (isset($where['admin_id']) && !empty($where['admin_id'])) $model = $model->where("admin_id",$where['admin_id']);
        $model->field(['admin_name,active_name,ip,create_time,user_agent']);
        $model->order("create_time desc");
        $count = self::counts($model);
        $model = $model->page($where['page'],$where['limit']);
        $data = $model->select();
        return compact('data','count');
    }
}
