<?php


namespace app\model\admin;


use app\model\FrameModel;
use app\model\ToolsTrait;

/**
 * 菜单控制
 * Class AdminMenu
 * @package app\model\admin
 */
class AdminMenu extends FrameModel
{
    use ToolsTrait;

    /**
     * 获取菜单id 找不到是返回 -1
     * @param string $module
     * @param string $controller
     * @param string $action
     * @return int
     */
    public static function getMenuId(string $module, string $controller,string $action): int
    {
        return self::where("module",$module)->where("controller",$controller)->where("action",$action)->value('id') ?: -1;
    }

    /**
     * 获取菜单 老接口速度太慢
     * @param int $pid
     * @param array $menu
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getMenu(int $pid = 0, array $menu = []): array
    {
        $model = new self;
        $model = $model->where("is_menu",1);
        $model = $model->where("status",1);
        $model = $model->where("pid",$pid);
        if ($menu != []) $model = $model->where("id",'in',$menu);
        $model = $model->field(['name','icon','component','is_show','path','id','title','pid','is_category']);
        $model = $model->order(["rank desc","id"]);
        $data = $model->select()->each(function ($item) use ($menu)
        {
            // 显示还是隐藏
            if (!$item['is_show']) $item['hidden'] = true;
            $item['children'] = self::getMenu($item['id'], $menu);
            // 顶层pid=0 最底层的children为空
            if ($item['is_category'])
            {
                $item['meta'] = ['title'=>$item['title'],'icon'=>$item['icon']];
                $item['path'] = "/".$item['path'];
                unset($item['title'],$item['icon'],$item['is_show'],$item['id'],$item['pid'],$item['name'],$item['is_category']);
            }else
            {
                if ($item['pid'] && empty($item['children']))
                {
                    if (empty($item['children'])) unset($item['children']);
                    else $item['alwaysShow'] = true;
                    // 次级目录
                    $item['meta'] = ['title'=>$item['title'],'icon'=>$item['icon']];
                }
                else
                {
                    $item['children'] = [[
                        'path' => $item['path'],
                        'name' => $item['name'],
                        'meta' => ['title'=>$item['title'],'icon'=>$item['icon']],
                        'component' => $item['component']
                    ]];
                    $item['component'] = "Layout";
                    unset($item['name']);
                    $item['path'] = "/".$item['path'];
                }
                unset($item['title'],$item['icon'],$item['is_show'],$item['id'],$item['pid']);
            }
        });
        return $data->toArray() ?: [];
    }

    /**
     * 获取菜单
     * @param array $menu
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getTreeMenu(array $menu = []): array
    {
        $model = new self;
        $model = $model->where("is_menu",1);
        $model = $model->where("status",1);
        if ($menu != []) $model = $model->where("id",'in',$menu);
        $model = $model->field(['name','icon','component','is_show','path','id','title','pid','is_category','no_cache']);
        $model = $model->order(["rank desc","id"]);
        $data = $model->select();
        if ($data)
        {
            $data = $data->toArray();
            $data = self::buildMenuTree($data);
        }
        return $data;
    }

    /**
     * 生成菜单树
     * @param $data
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function buildMenuTree($data)
    {
        $trees = array();
        foreach ($data as $item)
        {
            $tmp['meta']['title'] = $item['title'];
            $tmp['meta']['icon'] = $item['icon'];
            $tmp['meta']['noCache'] = $item['no_cache'] == 1;
            $tmp['pid'] = $item['pid'];
            $tmp['name'] = $item['name'];
            $tmp['path'] = $item['path'];
            $tmp['component'] = $item['component'];
            $tmp['alwaysShow'] = false;
            //目录
            if ($item['is_category'] == 1) {
                $tmp['alwaysShow'] = true;
                $tmp['path'] = "/".$item['path'];
                unset($item['name'],$item['is_category']);
            }
            if (!$item['is_show']) {
                $tmp['hidden'] = true;
            }
            $trees[$item['id']] = $tmp;
            unset($item['title'],$item['icon'],$item['is_show'],$item['pid']);
        }
        $tree = array();
        foreach ($trees as $k => $item) {
            if (isset($trees[$item['pid']])) {
                $trees[$item['pid']]['children'][] = &$trees[$k];
            } else {
                $tree[] = &$trees[$k];
            }
        }
        return $tree;
    }

    /**
     * 获取菜单
     * @param int $pid
     * @param array $menu
     * @param array $where
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function allMenus(int $pid = 0, array $menu = [], array $where = []): array
    {
        $model = new self;
        $model = $model->where("is_menu",1);
        if (isset($where['status']) && $where['status'] != '') $model = $model->where("status", $where['status']);
        if (isset($where['is_category']) && $where['is_category'] != '') $model = $model->where("is_category", $where['is_category']);
        if (isset($where['is_show']) && $where['is_show'] != '') $model = $model->where("is_show", $where['is_show']);
        if (isset($where['title'])) $model = $model->where("title", "like", "%$where[title]%");
        if (isset($where['name'])) $model = $model->where("name", "like", "%$where[name]%");
        $model = $model->where("pid",$pid);
        if ($menu != []) $model = $model->where("id",'in',$menu);
        $model = $model->order(["rank desc","id"]);
        $data = $model->select()->each(function ($item) use ($menu,$where)
        {
            $item['hasChildren'] = self::isChirdMenu($item['id'],$menu,$where);
        });
        return $data->toArray() ?: [];
    }

    /**
     * 是否存在子目录
     * @param $pid
     * @param $menu
     * @param $where
     * @return bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function isChirdMenu($pid,$menu,$where)
    {
        $model = new self;
        $model = $model->where("is_menu",1);
        if (isset($where['status']) && $where['status'] != '') $model = $model->where("status", $where['status']);
        if (isset($where['is_category']) && $where['is_category'] != '') $model = $model->where("is_category", $where['is_category']);
        if (isset($where['is_show']) && $where['is_show'] != '') $model = $model->where("is_show", $where['is_show']);
        if (isset($where['title'])) $model = $model->where("title", "like", "%$where[title]%");
        if (isset($where['name'])) $model = $model->where("name", "like", "%$where[name]%");
        if ($menu != []) $model = $model->where("id",'in',$menu);
        return $model->where("pid",$pid)
            ->find() ? true : false;
    }

    /**
     * 获取选择数据
     * @param int $pid
     * @param array $menu
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function lst(int $pid = 0, array $menu = []): array
    {
        $model = new self;
        $model = $model->where("pid",$pid);
        if ($menu != []) $model = $model->where("id",'in',$menu);
        $model = $model->field(['title','id','title as label']);
        $model = $model->order(["rank desc","id"]);
        $data = $model->select()->each(function ($item) use ($menu)
        {
            $item['children'] = self::lst($item['id'],$menu);
        });
        return $data->toArray() ?: [];
    }

    /**
     * 遍历选择项
     * @param array $data
     * @param $list
     * @param int $num
     * @param bool $clear
     */
    public static function myOptions(array $data, &$list, $num = 0, $clear=true)
    {
        foreach ($data as $k=>$v)
        {
            $list[] = ['value'=>$v['id'],'label'=>self::cross($num).$v['title']];
            if (is_array($v['children']) && !empty($v['children'])) {
                self::myOptions($v['children'],$list,$num+1,false);
            }
        }
    }

    /**
     * 返回选择项
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function returnOptions(int $pid = 0, array $menu = []): array
    {
        $list = [];
        $list[] = ['value'=>0,'label'=>'总后台'];
        self::myOptions(self::lst($pid, $menu),$list, 1, true);
        return $list;
    }
}
