<?php


namespace app\model\admin;


use app\model\FrameModel;
use app\model\ToolsTrait;

/**
 * Class AdminAuth
 * @package app\model\admin
 */
class AdminAuth extends FrameModel
{
    use ToolsTrait;

    /**
     * 获取权限id 找不到是返回 -1
     * @param string $module
     * @param string $controller
     * @param string $action
     * @return int
     */
    public static function getAuthId(string $module, string $controller,string $action): int
    {
        return self::where("module",$module)->where("controller",$controller)->where("action",$action)->value('id') ?: -1;
    }

    /**
     * 获取权限列表
     * @param int $pid
     * @param array $auth
     * @param array $where
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function allMenus(int $pid = 0, array $auth = [], array $where = []): array
    {
        $model = new self;
        if (isset($where['status']) && $where['status'] != '') $model = $model->where("status", $where['status']);
        if (isset($where['is_category']) && $where['is_category'] != '') $model = $model->where("is_category", $where['is_category']);
        if (isset($where['name'])) $model = $model->where("name", "like", "%$where[name]%");
        $model = $model->where("pid",$pid);
        if ($auth != []) $model = $model->where("id",'in',$auth);
        $model = $model->order(["rank desc","id"]);
        $data = $model->select()->each(function ($item) use ($auth,$where)
        {
            $item['hasChildren'] = self::isChirdAuth($item['id'],$auth,$where);
        });
        return $data->toArray() ?: [];
    }

    /**
     * 是否存在子目录
     * @param $id
     * @param $auth
     * @param $where
     * @return bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function isChirdAuth($id,$auth,$where)
    {
        $model = new self;
        if (isset($where['status']) && $where['status'] != '') $model = $model->where("status", $where['status']);
        if (isset($where['is_category']) && $where['is_category'] != '') $model = $model->where("is_category", $where['is_category']);
        if (isset($where['name'])) $model = $model->where("name", "like", "%$where[name]%");
        if ($auth != []) $model = $model->where("id",'in',$auth);
        return $model->where("pid",$id)
            ->find() ? true : false;
    }

    /**
     * 获取选择数据
     * @param int $pid
     * @param array $auth
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function lst(int $pid = 0, array $auth = []): array
    {
        $model = new self;
        $model = $model->where("pid",$pid);
        if ($auth != []) $model = $model->where("id",'in',$auth);
        $model = $model->field(['id','name', 'name as label']);
        $model = $model->order(["rank desc","id"]);
        $data = $model->select()->each(function ($item) use ($auth)
        {
            $item['children'] = self::lst($item['id'],$auth);
        });
        return $data->toArray() ?: [];
    }

    /**
     * 遍历选择项
     * @param array $data
     * @param $list
     * @param int $num
     * @param bool $clear
     */
    public static function myOptions(array $data, &$list, $num = 0, $clear=true)
    {
        foreach ($data as $k=>$v)
        {
            $list[] = ['value'=>$v['id'],'label'=>self::cross($num).$v['name']];
            if (is_array($v['children']) && !empty($v['children'])) {
                self::myOptions($v['children'],$list,$num+1,false);
            }
        }
    }

    /**
     * 返回选择项
     * @param int $pid
     * @param array $auth
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function returnOptions(int $pid = 0, array $auth = []): array
    {
        $list = [];
        $list[] = ['value'=>0,'label'=>'总后台'];
        self::myOptions(self::lst($pid, $auth),$list, 1, true);
        return $list;
    }

    /**
     * 获取无限极权限名称
     * @param $pid
     * @param $value
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getAuthName($pid, $value)
    {
        $model = new self;
        $model = $model->where("id",$pid);
        $model->field(["name",'pid']);
        $info = $model->find();
        if (!$info) return "操作位置未知" . '/' .$value;
        if ($info['pid'] != 0) return self::getAuthName($info['pid'], $info['name'] . '/' . $value);
        else return $info['name'] . '/' . $value;
    }

    /**
     * 获取操作记录
     * @param string $module
     * @param string $controller
     * @param string $action
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getLogName(string $module,string $controller,string $action)
    {
        $model = new self;
        $model = $model->where("module",$module);
        $model = $model->where("controller",$controller);
        $model = $model->where("action",$action);
        $model->field(["name",'pid']);
        $info = $model->find();
        if (!$info) return "未知操作";
        return self::getAuthName($info['pid'],$info['name']);
    }
}
