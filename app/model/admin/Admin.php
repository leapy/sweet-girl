<?php


namespace app\model\admin;


use app\model\FrameModel;
use app\model\ToolsTrait;

/**
 * 管理员表
 * Class Admin
 * @package app\model\admin
 */
class Admin extends FrameModel
{
    use ToolsTrait;

    /**
     * 根据用户Id获取用户信息
     * @param int $id
     * @return array|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getAdminInfo(int $id)
    {
        return self::where('id',$id)->find();
    }

    /**
     * 用户列表
     * @param array $where
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function lst(array $where): array
    {
        $model = new self;
        if (isset($where['tel']) && !empty($where['tel'])) $model = $model->where("tel","like","%$where[tel]%");
        if (isset($where['name']) && !empty($where['name'])) $model = $model->where("name","like","%$where[name]%");
        if (isset($where['email']) && !empty($where['email'])) $model = $model->where("email","like","%$where[email]%");
        if (isset($where['uid']) && !empty($where['uid'])) $model = $model->where("uid","like","%$where[uid]%");
        if (isset($where['role_id']) && !empty($where['role_id'])) $model = $model->where("role_id",$where["role_id"]);
        if (isset($where['status']) && $where['status'] != "") $model = $model->where("status",$where["status"]);
        $model = $model->page($where['page'],$where['limit']);
        $data = $model->select()->each(function ($item){
            $item['role_name'] = AdminRole::getNameById($item['role_id']);
        });
        return $data ? $data->toArray() : [];
    }
}
