<?php


namespace app\model\admin;


use app\model\FrameModel;
use app\model\ToolsTrait;

/**
 * 角色
 * Class AdminRole
 * @package app\model\admin
 */
class AdminRole extends FrameModel
{
    use ToolsTrait;

    /**
     * 获取权限
     * @param int $id
     * @return string
     */
    public static function getAuth(int $id): string
    {
        return self::where("id",$id)->value("auth") ?: '';
    }

    /**
     * 获取目录
     * @param int $id
     * @return string
     */
    public static function getMenu(int $id): string
    {
        return self::where("id",$id)->value("menu") ?: '';
    }

    /**
     * 角色列表
     * @param $pid
     * @param array $auth
     * @param array $where
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function roles($pid = '', array $auth = [], array $where)
    {
        $model = new self;
        $model = $model->where("pid",$pid);
        if (isset($where['name']) && $where['name'] != '') $model = $model->where("name",'like', "%$where[name]%");
        if (isset($where['status']) && $where['status'] != '') $model = $model->where("status",$where['status']);
        $model = $model->order(["rank desc","id"]);
        $data = $model->select()->each(function ($item) use ($auth, $where)
        {
            $item['children'] = self::roles($item['id'], $auth, $where);
        });
        return $data->toArray() ?: [];
    }

    /**
     * 获取选择数据
     * @param int $pid
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function lst(int $pid = 0): array
    {
        $model = new self;
        $model = $model->where("pid",$pid);
        $model = $model->field(['name','id as value']);
        $model = $model->order(["rank desc","id"]);
        $data = $model->select()->each(function ($item)
        {
            $item['children'] = self::lst($item['value']);
        });
        return $data->toArray() ?: [];
    }

    /**
     * 遍历选择项
     * @param array $data
     * @param $list
     * @param int $num
     * @param bool $clear
     */
    public static function myOptions(array $data, &$list, $num = 0, $clear=true)
    {
        foreach ($data as $k=>$v)
        {
            $list[] = ['value'=>$v['value'],'label'=>self::cross($num).$v['name']];
            if (is_array($v['children']) && !empty($v['children'])) {
                self::myOptions($v['children'],$list,$num+1,false);
            }
        }
    }

    /**
     * 返回选择项
     * @param int $pid
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function returnOptions(int $pid = 0): array
    {
        $list = [];
        if (self::where("id",$pid)->value("pid") == 0) $list[] = ['label'=>'总公司','value'=>0];
        $list[] = ['label'=>"|--".self::where("id",$pid)->value("name"),'value'=>$pid];
        self::myOptions(self::lst($pid),$list, 2, true);
        return $list;
    }

    /**
     * 横线
     * @param int $num
     * @return string
     */
    public static function cross(int $num=0): string
    {
        $str = "";
        if ($num == 1) $str .= "|--";
        elseif ($num > 1) for($i=0;$i<$num;$i++)
            if ($i==0) $str .= "|--";
            else $str .= "--";
        return $str." ";
    }

    /**
     * 获取角色名称
     * @param int $id
     * @return string
     */
    public static function getNameById(int $id): string
    {
        if ($id == 0) return "总公司";
        return self::where("id",$id)->value("name");
    }
}
