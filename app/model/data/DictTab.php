<?php


namespace app\model\data;


use app\model\FrameModel;
use app\model\ToolsTrait;

/**
 * Class DictTab
 * @package app\model\data
 */
class DictTab extends FrameModel
{
    use ToolsTrait;
}
