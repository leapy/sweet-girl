<?php


namespace app\model\data;


use app\model\FrameModel;
use app\model\ToolsTrait;

/**
 * Class Dict
 * @package app\model\data
 */
class Dict extends FrameModel
{
    use ToolsTrait;
}
