<?php


namespace app\model\data;


use app\model\FrameModel;

/**
 * 地区编码
 * Class Address
 * @package app\model\data
 */
class Address extends FrameModel
{
    /**
     * 地区列表
     * @param array $where
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function lst(array $where)
    {
        $model = new self;

        if (!empty($where['name']) && $where['name'] != "") $model = $model->where("name","like", "%$where[name]%");

        if (!empty($where['code']) && $where['code'] != "") $model = $model->where("code","like", "%$where[code]%");

        $count = self::counts($model);
        $model = $model->page((int)$where['page'], (int)$where['limit']);
        $data = $model->select();
        if ($data) $data = $data->toArray();
        return compact("data","count");
    }
}
