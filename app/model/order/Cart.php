<?php


namespace app\model\order;


use app\model\FrameModel;
use app\model\ToolsTrait;

/**
 * 购物车
 * Class Cart
 * @package app\model\order
 */
class Cart extends FrameModel
{
    use ToolsTrait;
}
