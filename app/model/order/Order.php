<?php


namespace app\model\order;


use app\model\FrameModel;
use app\model\ToolsTrait;

/**
 * 订单
 * Class Order
 * @package app\model\order
 */
class Order extends FrameModel
{
    use ToolsTrait;
}
