<?php


namespace app\model\system;

use app\model\FrameModel;
use app\model\ToolsTrait;

/**
 * 系统配置目录
 * Class SystemConfigTab
 * @package app\model\system
 */
class SystemConfigTab extends FrameModel
{
    use ToolsTrait;
}
