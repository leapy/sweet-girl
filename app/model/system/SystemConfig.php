<?php


namespace app\model\system;


use app\model\FrameModel;
use app\model\ToolsTrait;

/**
 * 系统配置
 * Class SystemConfig
 * @package app\model\system
 */
class SystemConfig extends FrameModel
{
    use ToolsTrait;

    /**
     * 获取值内容
     * @param $names
     * @return array|mixed|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getValueByNames($names)
    {
        if (is_array($names))
        {
            $_data = [];
            $data = (new self)->where("form_name","in", $names)->field(["value","form_name"])->select();
            if($data)
            {
                $data = $data->toArray();
                foreach ($data as $k => $v) $_data[$v['form_name']] = $v['value'];
            }
            return $_data;
        }
        else return (new self)->where("form_name", $names)->value("value") ?: "";
    }
}
