<?php
/**
 *
 * User: cfn
 * Date: 2020/10/26
 * Email: <cfn@leapy.cn>
 */

namespace app\model\product;

use app\model\FrameModel;
use app\model\ToolsTrait;

/**
 * 品牌
 * Class ProductBrand
 * @package app\model\product
 */
class ProductBrand extends FrameModel
{
    use ToolsTrait;

    /**
     * 所有菜单选项
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function choice()
    {
        $data = self::where("status",1)
            ->order("id desc")
            ->order("rank desc")
            ->field(["id,name"])
            ->select();
        return $data ? $data->toArray() : [];
    }
}
