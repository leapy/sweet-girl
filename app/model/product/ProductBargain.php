<?php


namespace app\model\product;

use app\model\FrameModel;
use app\model\ToolsTrait;

/**
 * 拼团
 * Class ProductBargain
 * @package app\model\product
 */
class ProductBargain extends FrameModel
{
    use ToolsTrait;
}
