<?php


namespace app\model\product;


use app\model\FrameModel;
use app\model\ToolsTrait;

/**
 * 商品表
 * Class Product
 * @package app\model\product
 */
class Product extends FrameModel
{
    use ToolsTrait;

    /**
     * @param array $where
     * @return array
     */
    public static function lst(array $where)
    {
        $condition = ['name' => 'like'];
        switch ($where['type'])
        {
            case 2:
                $where['status'] = 2;
                break;
            case 3:
                $where['stock'] = 0;
                break;
            case 4:
                $where['stock'] = "< stock_warn";
                break;
            case 5:
                $where['status'] = 3;
                break;
            default:
                $where['status'] = 1;
                break;
        }
        unset($where['type']);
        return self::list($where,$condition);
    }

    /**
     * 各种类别商品总数
     * @return mixed
     */
    public static function productSum()
    {
        $data[1] = self::comModel()
            ->where("status",1)
            ->count();
        $data[2] = self::comModel()
            ->where("status",2)
            ->count();
        $data[3] = self::comModel()
            ->where("stock",0)
            ->count();
        $data[4] = self::comModel()
            ->where("stock","< stock_warn")
            ->count();
        $data[5] = self::comModel()
            ->where("status",3)
            ->count();
        return $data;
    }

    /**
     * comModel
     * @return Product
     */
    private static function comModel()
    {
        return self::where("is_del",0);
    }
}
