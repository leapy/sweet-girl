<?php


namespace app\model\product;

use app\model\ToolsTrait;

/**
 * 商品规格值
 * Class ProductAttrsValue
 * @package app\model\product
 */
class ProductAttrsValue
{
    use ToolsTrait;
}
