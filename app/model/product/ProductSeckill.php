<?php


namespace app\model\product;


use app\model\FrameModel;
use app\model\ToolsTrait;

/**
 * 秒杀商品
 * Class ProductSeckill
 * @package app\model\product
 */
class ProductSeckill extends FrameModel
{
    use ToolsTrait;
}
