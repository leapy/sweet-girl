<?php


namespace app\model\product;


use app\model\FrameModel;
use app\model\ToolsTrait;

/**
 * 拼团商品
 * Class ProductOrder
 * @package app\model\product
 */
class ProductGroup extends FrameModel
{
    use ToolsTrait;
}
