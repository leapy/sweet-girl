<?php


namespace app\model\product;


use app\model\FrameModel;
use app\model\ToolsTrait;

/**
 * 商品分类
 * Class ProductCate
 * @package app\model\product
 */
class ProductCate extends FrameModel
{
    use ToolsTrait;

    /**
     * 加载列表
     * @param array $where
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function lst(array $where)
    {
        $model = new self;
        if ($where['cate_name'] != '') $model = $model->where("cate_name","like","%$where[cate_name]%");
        if ($where['status'] != '') $model = $model->where("status",$where['status']);
        $model = $model->where("pid",$where['pid']);
        $count = self::counts($model);
        $model = $model->order("rank desc");
        $model = $model->order('id desc');
        if ($where['pid'] == 0) $model = $model->page($where['page'],$where['limit']);
        $data = $model->select()->each(function ($item){
            $item['hasChildren'] = self::where("pid",$item['id'])->find() ? true : false;
            $item['pid_name'] = self::getPidName($item['pid']);
        });
        return compact("data","count");
    }

    /**
     * 获取上级菜单
     * @param int $pid
     * @return mixed|string
     */
    public static function getPidName(int $pid)
    {
        if ($pid == 0) return "顶级菜单";
        return self::where("id",$pid)->value("cate_name");
    }

    /**
     * 下级菜单
     * @param $id
     * @param string $prefix
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function getSubCate($id, $prefix="")
    {
        $data = self::where("status",1)
            ->where("pid",$id)
            ->field(['id,cate_name,pid'])
            ->select()->each(function ($item) use ($prefix){
                $item['cate_name'] = $prefix.$item['cate_name'];
            });
        return $data ? $data->toArray() : [];
    }

    /**
     * 选项
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function choice()
    {
        $pCate = self::where("status",1)
            ->where("pid",0)
            ->field(['id,cate_name,pid'])
            ->select();
        if ($pCate) $pCate = $pCate->toArray();
        $cate = [];
        foreach ($pCate as $item)
        {
            $cate[] = $item;
            $cate = array_merge($cate,self::getSubCate($item['id'],"|-- "));
        }
        return $cate;
    }
}
