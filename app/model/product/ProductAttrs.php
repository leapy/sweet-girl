<?php


namespace app\model\product;


use app\model\FrameModel;
use app\model\ToolsTrait;

/**
 * 商品规格
 * Class ProductAttrs
 * @package app\model\product
 */
class ProductAttrs extends FrameModel
{
    use ToolsTrait;
}
