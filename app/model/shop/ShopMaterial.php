<?php


namespace app\model\shop;


use app\model\FrameModel;
use app\model\ToolsTrait;

/**
 * 店铺资质
 * Class ShopMaterial
 * @package app\model\shop
 */
class ShopMaterial extends FrameModel
{
    use ToolsTrait;
}
