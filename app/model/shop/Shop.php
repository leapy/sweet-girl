<?php


namespace app\model\shop;


use app\model\FrameModel;
use app\model\ToolsTrait;

/**
 * 店铺
 * Class Shop
 * @package app\model\shop
 */
class Shop extends FrameModel
{
    use ToolsTrait;

    /**
     * 列表
     * @param array $where
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function lst(array $where)
    {
        $model = new self;
        $model = $model->where("status","in",[1,3]);
        $model = $model->where("is_del",0);
        if (isset($where['shop_id']) && $where['shop_id'] != "")
            $model = $model->where("shop_id",$where['shop_id']);
        if (isset($where['shop_name']) && $where['shop_name'] !="")
            $model = $model->where("shop_name","like","%$where[shop_name]%");
        if (isset($where['status'])  && $where['status'] != "")
            $model = $model->where("status",$where['status']);
        $count = self::counts($model);
        $model = $model->page($where['page'],$where['limit']);
        $model = $model->field(['shop_id,shop_name,principal,id,update_time,shop_image,status,remark']);
        $data = $model->select();
        if ($data) $data = $data->toArray();
        return compact("data","count");
    }
}
