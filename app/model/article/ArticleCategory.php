<?php


namespace app\model\article;


use app\model\FrameModel;
use app\model\ToolsTrait;

/**
 * Class ArticleCategory
 * @package app\model\article
 */
class ArticleCategory extends FrameModel
{
    use ToolsTrait;
}
