<?php


namespace app\model\article;


use app\model\FrameModel;
use app\model\ToolsTrait;

/**
 * Class Article
 * @package app\model\article
 */
class Article extends FrameModel
{
    use ToolsTrait;
}
