<?php


namespace app\model\file;


use app\model\FrameModel;
use app\model\ToolsTrait;

/**
 * Class FileTab
 * @package app\model\file
 */
class FileTab extends FrameModel
{
    use ToolsTrait;

    /**
     * 获取选择数据
     * @param int $pid
     * @param string $type
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function lst(int $pid = 0, string $type = "image"): array
    {
        $model = new self;
        $model = $model->where("pid",$pid);
        $model = $model->where("type",$type);
        $model = $model->field(['name','id','name as label','pid']);
        $data = $model->select()->each(function ($item) use ($type)
        {
            $item['children'] = self::lst($item['id'], $type);
        });
        return $data->toArray() ?: [];
    }

    /**
     * 遍历选择项
     * @param array $data
     * @param $list
     * @param int $num
     * @param string $type
     */
    public static function myOptions(array $data, &$list, $num = 0, $type = "image")
    {
        foreach ($data as $k=>$v)
        {
            $list[] = ['value'=>$v['id'],'label'=>self::cross($num).$v['name']];
            if (is_array($v['children']) && !empty($v['children'])) {
                self::myOptions($v['children'],$list,$num+1,$type);
            }
        }
    }

    /**
     * 返回选择项
     * @param int $pid
     * @param string $type
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function returnOptions(int $pid = 0, string $type = "image"): array
    {
        $list = [];
        $list[] = ['value'=>0,'label'=>'一级目录'];
        self::myOptions(self::lst($pid),$list, 1, $type);
        return $list;
    }
}
