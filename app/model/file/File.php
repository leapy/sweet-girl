<?php


namespace app\model\file;


use app\model\FrameModel;
use app\model\ToolsTrait;

/**
 * Class File
 * @package app\model\file
 */
class File extends FrameModel
{
    use ToolsTrait;

    /**
     * 分页数据
     * @param array $where
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function dataRows(array $where)
    {
        $model = new self;
        $model = $model->where("type",$where['type']);
        if ($where['tab_id'] != '') $model = $model->where("tab_id",$where['tab_id']);
        $count = self::counts($model);
        $data = $model->page($where['page'],$where['limit'])->order("add_time desc")->select();
        return compact("data","count");
    }
}
