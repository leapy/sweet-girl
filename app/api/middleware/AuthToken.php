<?php


namespace app\api\middleware;

use Closure;
use link\tools\Jwt;
use app\Request;
use Psr\SimpleCache\InvalidArgumentException;
use think\facade\Cache;

/**
 * token中间件
 * Class AuthToken
 * @package app\admin\middleware
 */
class AuthToken
{
    /**
     * 验证访问权限
     * @access public
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->header('Authorization');
        if (!$token) return app("json")->error("请求头错误！");
        try {
            $userInfo = Jwt::parserToken($token);
            if (!is_null($userInfo) && is_array($userInfo)) {
                $cache_token = Cache::store("redis")->get(md5("u".$userInfo['uid']));
                if (empty($cache_token)) return app("json")->warning("登录已过期");
                // 验证账号是否在别处登录
                if ($cache_token != $token) return app("json")->warning("账号已在别处登录！");
                // 设置用户信息
                Request::macro('userInfo', function () use (&$userInfo) {
                    return $userInfo;
                });
                // 设置用户登录状态
                Request::macro('isLogin', function () use (&$userInfo) {
                    return !is_null($userInfo);
                });
                // 设置用户ID
                Request::macro('uid', function () use (&$userInfo) {
                    return is_null($userInfo) ? 0 : $userInfo['uid'];
                });
            }else return app("json")->fail("token签发有误！");
        }catch (\Exception $e)
        {
            return app("json")->fail($e->getMessage());
        }
        return $next($request);
    }
}
