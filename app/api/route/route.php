<?php

use think\facade\Route;

// miss路由
Route::miss(function() {
    return app('json')->fail('找不到该路由地址，请联系开发人员！');
});

// 无需登录的路由
Route::group(function () {
    // 验证码
    Route::post('/verify', 'api/index/verify');
    // 登录
    Route::post('/login', 'api/user.user/login');
    // 注册
    Route::post('/register', 'api/user.user/register');
    // 找回密码
    Route::post('/retrieve', 'api/user.user/retrieve');

})->middleware(\think\middleware\AllowCrossDomain::class);

// 需要登录登录的路由
Route::group(function () {

})->middleware(\think\middleware\AllowCrossDomain::class)->middleware(\app\api\middleware\AuthToken::class);
