<?php


namespace app\api\controller;

use app\BaseController;
use app\model\admin\AdminAuth;
use think\facade\App;

/**
 * 权限控制中间件
 * Class AuthController
 * @package app\admin\controller
 */
class ApiController extends BaseController
{
    /**
     * 用户ID
     * @var null
     */
    protected $uid;

    /**
     * 用户信息
     * @var
     */
    protected $userInfo;

    /**
     * model
     * @var
     */
    protected $model = null;

    /**
     * @var string
     */
    protected $module;

    /**
     * @var string
     */
    protected $controller;

    /**
     * @var string
     */
    protected $action;

    /**
     * 是否登录
     * @var
     */
    protected $isLogin;

    /**
     * Trait
     */
    use ControllerTrait;

    /**
     * 初始化
     */
    protected function initialize()
    {
        parent::initialize(); // TODO: Change the autogenerated
        $this->module = App::getInstance()->http->getName();
        $this->controller = unCamelize($this->request->controller());
        $this->action = $this->request->action();
        $this->model = $this->buildModel();
        $this->tryGetUserInfo();
    }

    /**
     * 尝试获取用户信息
     */
    protected function tryGetUserInfo()
    {
        try {
            $this->uid = app('request')->uid();
            $this->userInfo = app('request')->userInfo();
            $this->isLogin = app('request')->isLogin();
        }catch (\Exception $e)
        {
            $this->uid = 0;
            $this->userInfo = [];
            $this->isLogin = false;
        }
    }

    /**
     * 生成model路径
     * @return object|\think\App|null
     */
    protected function buildModel()
    {
        $path = explode(".", $this->request->controller());
        $modelPath = "app\\model";
        foreach ($path as $v) $modelPath .= "\\".$v;
        if (class_exists($modelPath)) return app($modelPath);
        return null;
    }
}
