<?php
/**
 *
 * User: cfn
 * Date: 2020/11/7
 * Email: <cfn@leapy.cn>
 */

namespace app\api\controller\user;


use app\api\controller\ApiController;
use link\tools\Argv;
use link\tools\Jwt;
use think\facade\Cache;
use app\model\user\User as uModel;

/**
 * Class User
 * @package app\api\controller\user
 */
class User extends ApiController
{
    /**
     * 登录
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function login()
    {
        $data = Argv::post([
            ['tel',''],
            ['password','']
        ]);
        if (!$data['tel']) return app("json")->fail("手机号为空");
        if (!$data['password']) return app("json")->fail("密码为空");
        $info = uModel::where("tel",$data['tel'])->find();
        if (empty($info)) return app("json")->fail("账号不存在");
        if (md5(trim($data['password'])) != $info['password']) return app("json")->fail("密码不正确");
        $token = Jwt::createToken($info->toArray());
        Cache::store('redis')->set(md5("u".$info['uid']),$token,3600 * 24);
        return $token ? app("json")->success("登录成功",compact("token")) : app("json")->fail("登录失败");
    }

    /**
     * 注册
     * @return mixed
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function register()
    {
        $data = Argv::post([
            ['tel',''],
            ['code',''],
            ['password',''],
            ['pid',0],
            ['register_type','h5']
        ]);
        if (!$data['tel']) return app("json")->fail("手机号为空");
        if (!$data['code']) return app("json")->fail("验证码为空");
        if (!$data['password']) return app("json")->fail("密码为空");
        if (!Cache::store("redis")->has(md5($data['tel']."sms_register"))) return app("json")->fail("验证码已失效");
        if (md5(Cache::store("redis")->get(md5($data['tel']."sms_register"))) != md5($data['code'])) return app("json")->fail("验证码不正确");
        if (uModel::issetValue("tel",$data['tel'])) return app("json")->fail("手机号重复");
        $data['username'] = "昵称".waterNo(new uModel(),5);
        $uid = uModel::addUser($data);
        event("UserRegister",["uid"=>$uid]);
        return $uid ? app("json")->success("注册成功") : app("json")->fail("注册失败");
    }

    /**
     * 找回密码
     * @return mixed
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function retrieve()
    {
        $data = Argv::post([
            ['tel',''],
            ['code',''],
            ['password',''],
        ]);
        if (!$data['tel']) return app("json")->fail("手机号为空");
        if (!$data['code']) return app("json")->fail("验证码为空");
        if (!$data['password']) return app("json")->fail("密码为空");
        if (!Cache::store("redis")->has(md5($data['tel']."sms_retrieve"))) return app("json")->fail("验证码已失效");
        if (md5(Cache::store("redis")->get(md5($data['tel']."sms_retrieve"))) != md5($data['code'])) return app("json")->fail("验证码不正确");
        $uid = uModel::where("tel",$data['tel'])->update(['password'=>md5($data['password'])]);
        return $uid ? app("json")->success("设置成功") : app("json")->fail("设置失败");
    }
}
