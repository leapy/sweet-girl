<?php
namespace app\api\controller;

use link\tools\Argv;
use link\tools\Mail;
use link\tools\Sms;
use think\facade\Cache;
use think\facade\Config;

/**
 * API
 * Class Index
 * @package app\admin\controller
 */
class Index extends ApiController
{
    /**
     * 发送验证码
     * @return mixed
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function verify()
    {
        $data = Argv::post([
            ['tel',''],
            ['email',''],
            ['type','']
        ]);
        if (!$data['tel'] && !$data['email']) return app("json")->fail("参数有误！");
        if (!$data['type']) return app("json")->fail("请设置要发送的信息类型");
        $code = rand(1000,9999);
        switch ($data['type'])
        {
            case "sms_retrieve":
//                if (Cache::store("redis")->has(md5($data['tel']."sms_retrieve")))
//                {
//                    $code = Cache::store("redis")->get(md5($data['tel']."sms_retrieve"));
//                    $res = Sms::retrieve($data['tel'],$code);
//                }else
//                {
//                    Cache::store("redis")->set(md5($data['tel']."sms_retrieve"),$code,300);
//                    $res = Sms::retrieve($data['tel'],$code);
//                }
                Cache::store("redis")->set(md5($data['tel']."sms_retrieve"),1234,300);
                return true ? app("json")->success("发送成功") : app("json")->fail("发送失败");
            case "sms_register":
                if (Cache::store("redis")->has(md5($data['tel']."sms_register")))
                {
                    $code = Cache::store("redis")->get(md5($data['tel']."sms_retrieve"));
                    $res = Sms::register($data['tel'],$code);
                }else
                {
                    Cache::store("redis")->set(md5($data['tel']."sms_retrieve"),$code,300);
                    $res = Sms::register($data['tel'],$code);
                }
//                Cache::store("redis")->set(md5($data['tel']."sms_register"),1234,300);
                return $res ? app("json")->success("发送成功") : app("json")->fail("发送失败");
            case "mail_register":
                if (Cache::store("redis")->has(md5($data['email']."mail_register")))
                {
                    $code = Cache::store("redis")->get(md5($data['email']."mail_register"));
                    $res = Mail::register($data['email'],$code);
                }else
                {
                    Cache::store("redis")->set(md5($data['email']."mail_register"),$code,300);
                    $res = Mail::register($data['email'],$code);
                }
                return $res ? app("json")->success("发送成功") : app("json")->fail("发送失败");
            case "mail_retrieve":
                if (Cache::store("redis")->has(md5($data['email']."mail_retrieve")))
                {
                    $code = Cache::store("redis")->get(md5($data['email']."mail_retrieve"));
                    $res = Mail::retrieve($data['email'],$code);
                }else
                {
                    Cache::store("redis")->set(md5($data['email']."mail_retrieve"),$code,300);
                    $res = Mail::retrieve($data['email'],$code);
                }
                return $res ? app("json")->success("发送成功") : app("json")->fail("发送失败");
            default:
                return app("json")->fail("未找到要发送的信息类型");
        }
    }
}
