<?php
/**
 *
 * User: cfn
 * Date: 2020/10/26
 * Email: <cfn@leapy.cn>
 */

namespace app\admin\validate\product;


use think\Validate;

/**
 * Class Product
 * @package app\admin\validate\product
 */
class Product extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'shop_id' => 'require',
        'name' => 'require',
        'cate_id' => 'require',
        'info' => 'require',
        'image' => 'require',
        'price' => 'require',
        'is_discount' => 'require',
        'is_new' => 'require',
        'is_hot' => 'require',
        'stock' => 'require|number',
        'stock_warn' => 'require|number',
        'status' => 'require|number|in:0,1'
    ];

    /**
     * 提示消息
     */
    protected $message = [
        'shop_id.require' => '店铺必须选择！',
        'name.require' => '商品名称必须填写！',
        'cate_id.require' => '商品分类必须选择',
        'info.require' => '商品介绍必须选择！',
        'image.require' => '商品主图必须上传！',
        'price.require' => '商品价格必须填写!',
        'is_discount.require' => '是否折扣必须选择！',
        'is_new.require' => '是否新品必须选择！',
        'is_hot.require' => '是否热卖产品必须选择！',
        'stock.require' => '商品库存必须填写',
        'stock.number' => '商品库存必须是数字',
        'stock_warn.require' => '库存预警必须填写',
        'stock_warn.number' => '库存预警必须是数字',
        'status.require' => '商品状态必须选择',
        'status.in' => '商品状态必须是0，1，2',
    ];
}
