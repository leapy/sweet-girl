<?php
/**
 *
 * User: cfn
 * Date: 2020/10/26
 * Email: <cfn@leapy.cn>
 */

namespace app\admin\validate\product;


use think\Validate;

/**
 * 商品品牌
 * Class ProductBrand
 * @package app\admin\validate\product
 */
class ProductBrand extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'name' => 'require',
        'status' => 'require|number|in:0,1',
        'rank' => 'require|number|between:0,99',
    ];

    /**
     * 提示消息
     */
    protected $message = [
        'name.require' => '品牌名称必须填写！',
        'status.require' => '启用状态必须填写！',
        'status.number' => '启用状态必须是数字！',
        'status.in' => '启用状态是0或者1！',
        'rank.require' => '排序必须填写！',
        'rank.number' => '排序必须是数字！',
        'rank.between' => '排序在0-99之间！',
    ];
}
