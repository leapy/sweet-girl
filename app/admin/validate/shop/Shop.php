<?php
/**
 *
 * User: cfn
 * Date: 2020/10/29
 * Email: <cfn@leapy.cn>
 */

namespace app\admin\validate\shop;


use think\Validate;

/**
 * Class Shop
 * @package app\admin\validate\shop
 */
class Shop extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'shop_name' => 'require',
        'shop_image' => 'require',
        'principal' => 'require',
        'tel' => 'require',
        'idcard' => 'require'
    ];

    /**
     * 提示消息
     */
    protected $message = [
        'shop_name.require' => '店铺名称必须填写！',
        'shop_image.require' => '店铺图片必须上传！',
        'principal.require' => '店铺负责人姓名必须填写',
        'idcard.require' => '铺负责人身份证必须填写！',
        'tel.require' => '铺负责人手机号必须填写！',
    ];
}
