<?php


namespace app\admin\validate\admin;


use think\Validate;

/**
 * 菜单权限控制器
 * Class AdminAuth
 * @package app\admin\validate\admin
 */
class AdminMenu extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'title' => 'require',
    ];

    /**
     * 提示消息
     */
    protected $message = [
        'title.require' => '菜单名称必须填写！'
    ];
}
