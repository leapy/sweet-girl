<?php


namespace app\admin\validate\admin;


use think\Validate;

/**
 * 菜单权限控制器
 * Class AdminAuth
 * @package app\admin\validate\admin
 */
class AdminAuth extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'name' => 'require',
        'module' => 'require',
        'controller' => 'require',
        'action' => 'require',
    ];

    /**
     * 提示消息
     */
    protected $message = [
        'name.require' => '权限名称必须填写！',
        'module.require' => '模块名称必须填写！',
        'controller.require' => '控制器名称必须填写！',
        'action.require' => '方法名称必须填写！',
    ];
}
