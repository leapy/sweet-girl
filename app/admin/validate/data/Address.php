<?php


namespace app\admin\validate\data;

use think\Validate;

/**
 * 地址验证
 * Class Address
 * @package app\admin\validate\data
 */
class Address extends Validate
{
    /**
     * 验证规则
     */
    protected $rule = [
        'name' => 'require',
        'code' => 'require',
    ];

    /**
     * 提示消息
     */
    protected $message = [
        'name.require' => '区域名称必须填写！',
        'code.require' => '区域代码必须填写！'
    ];
}
