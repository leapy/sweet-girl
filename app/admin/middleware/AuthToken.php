<?php


namespace app\admin\middleware;

use Closure;
use link\tools\Jwt;
use app\Request;
use think\facade\Cache;

/**
 * token中间件
 * Class AuthToken
 * @package app\admin\middleware
 */
class AuthToken
{
    /**
     * 验证访问权限
     * @access public
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->header('Authorization');
        if (!$token) return app("json")->error("请求头错误！");
        try {
            $adminInfo = Jwt::parserToken($token);
            if (!is_null($adminInfo) && is_array($adminInfo)) {
                $cache_token = Cache::store("redis")->get(md5($adminInfo['id']));
                if (empty($cache_token)) return app("json")->warning("登录已过期");
                // 验证账号是否在别处登录
                if ($cache_token != $token) return app("json")->warning("账号已在别处登录！");
                // 设置用户信息
                Request::macro('adminInfo', function () use (&$adminInfo) {
                    return $adminInfo;
                });
                // 设置用户登录状态
                Request::macro('isLogin', function () use (&$adminInfo) {
                    return !is_null($adminInfo);
                });
                // 设置用户ID
                Request::macro('adminId', function () use (&$adminInfo) {
                    return is_null($adminInfo) ? 0 : $adminInfo['id'];
                });
            }else return app("json")->fail("token签发有误！");
        }catch (\Exception $e)
        {
            return app("json")->fail($e->getMessage());
        }
        return $next($request);
    }
}
