<?php

use think\facade\Route;

// miss路由
Route::miss(function() {
    return app('json')->fail('找不到该路由地址，请联系开发人员！');
});

// 无需登录的路由
Route::group(function () {

    Route::post('/test', 'Index/test'); // 测试
    Route::get('/test', 'Index/test'); // 测试

    Route::post('/login', 'Index/login'); // 登录

})->middleware(\think\middleware\AllowCrossDomain::class);

// 需要登录登录的路由
Route::group(function () {
    /**
     * 首页
     */
    Route::get('/auth', 'Index/auth');
    Route::post('/info', 'Index/info');
    Route::get('/logout', 'Index/logout');
    Route::post('/admin/changePassword', 'admin/admin.Admin/changePassword'); // 修改密码
    Route::get('/admin/log/lst', 'admin/admin.AdminLog/lst'); // 日志列表
    Route::get('/base/info', 'admin/admin.Admin/baseInfo');
    Route::post('/base/info/save', 'admin/admin.Admin/saveBaseInfo'); // 保存修改
    Route::get('/admin/security/info', 'admin/admin.Admin/securityInfo'); // 获取电话邮箱
    Route::post('/admin/modify/verify', 'admin/admin.Admin/verify'); // 发送验证信息
    Route::post('/admin/modify', 'admin/admin.Admin/modify'); // 保存修改邮箱手机号

    /**
     * 菜单
     */
    Route::get('/menu/list','admin/admin.AdminMenu/index');
    Route::put('/menu/field','admin/admin.AdminMenu/field');
    Route::post('/menu/submit','admin/admin.AdminMenu/submit');
    Route::delete('/menu/del','admin/admin.AdminMenu/del');
    Route::get('/menu/menu/option','admin/admin.AdminMenu/selectOptions');

    /**
     * 权限
     */
    Route::get('/auth/list','admin/admin.AdminAuth/index');
    Route::put('/auth/field','admin/admin.AdminAuth/field');
    Route::post('/auth/submit','admin/admin.AdminAuth/submit');
    Route::delete('/auth/del','admin/admin.AdminAuth/del');
    Route::get('/auth/option','admin/admin.AdminAuth/selectOptions');

    /**
     * 角色
     */
    Route::get('/role/list','admin/admin.AdminRole/index');
    Route::put('/role/field','admin/admin.AdminRole/field');
    Route::post('/role/submit','admin/admin.AdminRole/submit');
    Route::delete('/role/del','admin/admin.AdminRole/del');
    Route::get('/role/role/option','admin/admin.AdminRole/selectOptions');
    Route::get('/role/auth/option','admin/admin.AdminRole/authOptions');
    Route::get('/role/menu/option','admin/admin.AdminRole/menuOptions');

    /**
     * 管理员表
     */
    Route::post('/user/save', 'admin/admin.Admin/save');
    Route::get('/user/list','admin/admin.Admin/index');
    Route::put('/user/field','admin/admin.Admin/field');
    Route::delete('/user/del','admin/admin.Admin/del');
    Route::put('/user/enabled','admin/admin.Admin/enabled');
    Route::put('/user/disabled','admin/admin.Admin/disabled');

    /**
     * 地区编码
     */
    Route::get('/address/list','admin/data.Address/index');
    Route::delete('/address/del','admin/data.Address/del');
    Route::post('/address/save', 'admin/data.Address/save');
    Route::put('/address/field','admin/data.Address/field');

    /**
     * 系统配置目录
     */
    Route::get('/system/config/tab/list','admin/system.SystemConfigTab/lst');
    Route::delete('/system/config/tab/del','admin/system.SystemConfigTab/del');
    Route::post('/system/config/tab/save', 'admin/system.SystemConfigTab/save');
    Route::put('/system/config/tab/field','admin/system.SystemConfigTab/field');
    Route::get('/system/config/tab/options', 'admin/system.SystemConfigTab/options');
    Route::put('/system/config/tab/enabled','admin/system.SystemConfigTab/enabled');
    Route::put('/system/config/tab/disabled','admin/system.SystemConfigTab/disabled');

    /**
     * 系统配置
     */
    Route::get('/system/config/list','admin/system.SystemConfig/lst');
    Route::delete('/system/config/del','admin/system.SystemConfig/del');
    Route::post('/system/config/save', 'admin/system.SystemConfig/save');
    Route::put('/system/config/field','admin/system.SystemConfig/field');
    Route::put('/system/config/enabled','admin/system.SystemConfig/enabled');
    Route::put('/system/config/disabled','admin/system.SystemConfig/disabled');
    Route::get('/system/config/options','admin/system.SystemConfig/options');

    /**
     * 配置项配置
     */
    Route::get('/system/config/items','admin/system.SystemConfig/items'); // 获取所有配置项
    Route::post('/system/config/saveAll','admin/system.SystemConfig/saveAll'); // 保存所有配置项
    Route::get('/system/config/sendSms','admin/system.SystemConfig/sendSms'); // 短信测试
    Route::get('/system/config/sendEmail','admin/system.SystemConfig/sendEmail'); // 邮件测试
    Route::get('/system/config/emailTemp','admin/system.SystemConfig/emailTemp'); // 邮件模板
    Route::post('/system/config/saveEmailTemp','admin/system.SystemConfig/saveEmailTemp'); // 邮件模板

    /**
     * 上传
     */
    Route::post('/upload/image/one','admin/common.file/uploadImageOne'); // 上传图片
    Route::delete('/upload/image/del','admin/common.file/delImage'); // 测试删除

    /**
     * 字典目录
     */
    Route::get('/dict_tab/list','admin/data.DictTab/lst');
    Route::delete('/dict_tab/del','admin/data.DictTab/del');
    Route::post('/dict_tab/save', 'admin/data.DictTab/save');
    Route::put('/dict_tab/field','admin/data.DictTab/field');
    Route::put('/dict_tab/enabled','admin/data.DictTab/enabled');
    Route::put('/dict_tab/disabled','admin/data.DictTab/disabled');
    Route::get('/dict_tab/options','admin/data.DictTab/options');

    /**
     * 字典内容
     */
    Route::get('/dict/list','admin/data.Dict/lst');
    Route::delete('/dict/del','admin/data.Dict/del');
    Route::post('/dict/save', 'admin/data.Dict/save');
    Route::put('/dict/field','admin/data.Dict/field');
    Route::put('/dict/enabled','admin/data.Dict/enabled');
    Route::put('/dict/disabled','admin/data.Dict/disabled');

    /**
     * 字典内容
     */
    Route::get('/article/category/list','admin/article.ArticleCategory/lst');
    Route::delete('/article/category/del','admin/article.ArticleCategory/del');
    Route::post('/article/category/save', 'admin/article.ArticleCategory/save');
    Route::put('/article/category/field','admin/article.ArticleCategory/field');
    Route::put('/article/category/enabled','admin/article.ArticleCategory/enabled');
    Route::put('/article/category/disabled','admin/article.ArticleCategory/disabled');
    Route::get('/article/category/options','admin/article.ArticleCategory/options');

    /**
     * 字典内容
     */
    Route::get('/article/article/list','admin/article.Article/lst');
    Route::delete('/article/article/del','admin/article.Article/del');
    Route::post('/article/article/save', 'admin/article.Article/save');
    Route::put('/article/article/field','admin/article.Article/field');
    Route::put('/article/article/enabled','admin/article.Article/enabled');
    Route::put('/article/article/disabled','admin/article.Article/disabled');

    /**
     * 商品
     */
    Route::post('/product/save', 'admin/product.product/save');
    Route::get('/product/list','admin/product.product/lst');
    Route::put('/product/field','admin/product.product/field');
    Route::delete('/product/del','admin/product.product/del');
    Route::get('/product/counts','admin/product.product/counts');
    Route::get('/product/detail','admin/product.product/detail');

    /**
     * 店铺
     */
    Route::post('/shop/save', 'admin/shop.shop/save');
    Route::get('/shop/lst','admin/shop.shop/lst');
    Route::put('/shop/field','admin/shop.shop/field');
    Route::delete('/shop/del','admin/shop.shop/del');

    /**
     * 文件管理
     */
    Route::post('/file/menu/save', 'admin/common.file/saveMenu');
    Route::get('/file/menu/option','admin/common.file/menuOption');
    Route::get('/file/menu/lst','admin/common.file/menuLst');
    Route::delete('/file/menu/del','admin/common.file/delMenu');
    Route::get('/file/file/lst','admin/common.file/fileList');

    // 商品品牌
    Route::post('/product/brand/save', 'admin/product.productBrand/save');
    Route::get('/product/brand/lst','admin/product.productBrand/lst');
    Route::delete('/product/brand/del','admin/product.productBrand/del');
    Route::put('/product/brand/field','admin/product.productBrand/field');
    Route::get('/product/brand/choice','admin/product.productBrand/choice');

    // 商品分类
    Route::post('/product/cate/save', 'admin/product.productCate/save');
    Route::get('/product/cate/lst','admin/product.productCate/lst');
    Route::delete('/product/cate/del','admin/product.productCate/del');
    Route::get('/product/cate/options','admin/product.productCate/options');
    Route::get('/product/cate/choice','admin/product.productCate/choice');

})->middleware(\think\middleware\AllowCrossDomain::class)->middleware(\app\admin\middleware\AuthToken::class);
