<?php


namespace app\admin\controller\article;


use app\admin\controller\AuthController;
use link\tools\Argv;
use app\model\article\ArticleCategory as cModel;

/**
 * Class ArticleCategory
 * @package app\admin\controller\article
 */
class ArticleCategory extends AuthController
{
    /**
     * 列表
     * @return array
     */
    public function lst()
    {
        $where = Argv::get([
            ['id', ''],
            ['name', ''],
            ['code', ''],
            ['status', ''],
            ['page', 1],
            ['limit', 10],
        ]);
        $condition = ['name' => 'like'];
        return app("json")->success(cModel::list($where, $condition));
    }

    /**
     * 保存修改
     * @return mixed
     */
    public function save()
    {
        $data = Argv::post([
            ['id',''],
            ['name',''],
            ['code',''],
            ['remark',''],
            ['rank',1],
            ['status',1]
        ]);
        if ($data['name'] == "") return app("json")->fail("分类名称不能为空");
        if ($data['code'] == "") return app("json")->fail("分类编号不能为空");
        if ($data['id'] == "")
        {
            $data['create_user'] = $this->adminId;
            $data['create_time'] = time();
            $res = cModel::insert($data);
        }else
        {
            $data['update_user'] = $this->adminId;
            $data['update_time'] = time();
            $res = cModel::update($data, ['id' => $data['id']]);
        }
        return $res ? app("json")->success("操作成功") : app("json")->fail("操作失败");
    }

    /**
     * 配置项
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function options()
    {
        $data = cModel::where("status",1)
            ->order("rank desc")
            ->field("id,name")
            ->select();
        return app("json")->success($data ? $data->toArray() : []);
    }
}
