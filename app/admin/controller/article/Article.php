<?php


namespace app\admin\controller\article;


use app\admin\controller\AuthController;
use link\tools\Argv;
use app\model\article\Article as aModel;

/**
 * Class Article
 * @package app\admin\controller\article
 */
class Article extends AuthController
{
    /**
     * 列表
     * @return array
     */
    public function lst()
    {
        $where = Argv::get([
            ['id', ''],
            ['cid', ''],
            ['name', ''],
            ['code', ''],
            ['status', ''],
            ['page', 1],
            ['limit', 10],
        ]);
        $condition = ['name' => 'like'];
        return app("json")->success(aModel::list($where, $condition));
    }

    /**
     * 保存修改
     * @return mixed
     */
    public function save()
    {
        $data = Argv::post([
            ['id',''],
            ['cid',''],
            ['content',''],
            ['image',''],
            ['code',''],
            ['digest',''],
            ['remark',''],
            ['name',''],
            ['rank',1],
            ['status',1]
        ]);
        if ($data['name'] == "") return app("json")->fail("文章名称不能为空");
        if ($data['cid'] == "") return app("json")->fail("文章分类不能为空");
        if ($data['id'] == "")
        {
            $data['create_user'] = $this->adminId;
            $data['create_time'] = time();
            $res = aModel::insert($data);
        }else
        {
            $data['update_user'] = $this->adminId;
            $data['update_time'] = time();
            $res = aModel::update($data, ['id' => $data['id']]);
        }
        return $res ? app("json")->success("操作成功") : app("json")->fail("操作失败");
    }
}
