<?php


namespace app\admin\controller\shop;


use app\admin\controller\AuthController;
use link\tools\Argv;
use app\model\shop\Shop as sModel;
use app\admin\validate\shop\Shop as sValidate;

/**
 * Class Shop
 * @package app\admin\controller\shop
 */
class Shop extends AuthController
{
    /**
     * 列表
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function lst()
    {
        $where = Argv::get([
            ['shop_id',''],
            ['shop_name',''],
            ['status',''],
            ['page',1],
            ['limit',10]
        ]);
        return app("json")->success(sModel::lst($where));
    }

    /**
     * 保存
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function save()
    {
        $data = Argv::post([
            ['id',''],
            ['shop_id',''],
            ['shop_name',''],
            ['shop_image',''],
            ['principal',''],
            ['tel',''],
            ['idcard',''],
            ['status',''],
            ['remark','']
        ]);
        $sValidate = new sValidate();
        if (!$sValidate->check($data)) return app("json")->fail($sValidate->getError());
        if (empty($data['shop_id'])) $data['shop_id'] = waterNo(new sModel(),7,"M");
        if ($data['id'] != "" && sModel::where("is_del",0)->where("tel",$data['tel'])->where("id","<>",$data['id'])->find())
            return app("json")->fail("手机号存在重复");
        return sModel::submit($data) ? app("json")->success("操作成功") : app("json")->fail("操作失败");
    }
}
