<?php


namespace app\admin\controller\data;

use app\admin\controller\AuthController;
use link\tools\Argv;
use app\model\data\Address as aModel;
use app\admin\validate\data\Address as aValidate;

/**
 * 地区编码维护
 * Class Address
 * @package app\admin\controller\data
 */
class Address extends AuthController
{
    /**
     * 权限列表
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index()
    {
        $where = Argv::get([
            ['code',''],
            ['name',''],
            ['page', 1],
            ['limit', 10],
        ]);
        return app('json')->success(aModel::lst($where));
    }

    /**
     * 保存
     * @return mixed
     */
    public function save()
    {
        $data = Argv::post([
            ['id',''],
            ['name',''],
            ['code',''],
            ['rank',''],
        ]);
        $validate = new aValidate();
        $result = $validate->check($data);
        if(!$result){
            return app("json")->fail($validate->getError());
        }
        if ($data['id'] != "")
        {
            return aModel::where("id",$data['id'])->update($data) ? app("json")->success("修改成功") : app("json")->fail("修改失败");
        }else
        {
            return aModel::insert($data) ? app("json")->success("操作成功") : app("json")->fail("操作失败");
        }
    }
}
