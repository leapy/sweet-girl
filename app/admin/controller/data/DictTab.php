<?php


namespace app\admin\controller\data;


use app\admin\controller\AuthController;
use app\model\data\DictTab as tModel;
use link\tools\Argv;

/**
 * Class DictTab
 * @package app\admin\controller\data
 */
class DictTab extends AuthController
{
    /**
     * 列表
     * @return array
     */
    public function lst()
    {
        $where = Argv::get([
            ['name', ''],
            ['status', ''],
            ['page', 1],
            ['limit', 10],
        ]);
        $condition = ['name' => 'like'];
        return app("json")->success(tModel::list($where, $condition));
    }

    /**
     * 保存
     * @return mixed
     */
    public function save()
    {
        $data = Argv::post([
            ['id',''],
            ['name',''],
            ['remark',''],
            ['rank',''],
            ['status',''],
        ]);
        if ($data['name'] == "") return app("json")->fail("字典名称不能为空");
        if ($data['id'] == "")
        {
            $data['create_user'] = $this->adminId;
            $data['create_time'] = time();
            $res = tModel::insert($data);
        }else
        {
            $data['update_user'] = $this->adminId;
            $data['update_time'] = time();
            $res = tModel::update($data, ['id' => $data['id']]);
        }
        return $res ? app("json")->success("操作成功") : app("json")->fail("操作失败");
    }

    /**
     * 获取选择项
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function options()
    {
        $data = tModel::where("status",1)
            ->order("rank desc")
            ->field(['id','name'])
            ->select();
        return app("json")->success($data ?$data->toArray() : []);
    }
}
