<?php
/**
 *
 * User: cfn
 * Date: 2020/9/30
 * Email: <cfn@leapy.cn>
 */

namespace app\admin\controller\data;


use app\admin\controller\AuthController;
use think\facade\Db;

/**
 * 数据库维护
 * Class Sql
 * @package app\admin\controller\data
 */
class Sql extends AuthController
{
    /**
     * 列表
     */
    public function lst()
    {
        $data = Db::query("show COLUMNS FROM admin");
    }
}
