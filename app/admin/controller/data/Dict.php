<?php


namespace app\admin\controller\data;


use app\admin\controller\AuthController;
use link\tools\Argv;
use app\model\data\Dict as dModel;
/**
 * Class Dict
 * @package app\admin\controller\data
 */
class Dict extends AuthController
{
    /**
     * 列表
     * @return array
     */
    public function lst()
    {
        $where = Argv::get([
            ['tab_id', ''],
            ['label', ''],
            ['is_check', ''],
            ['status', ''],
            ['page', 1],
            ['limit', 10],
        ]);
        $condition = ['label' => 'like'];
        return app("json")->success(dModel::list($where, $condition));
    }

    /**
     * 保存
     * @return mixed
     */
    public function save()
    {
        $data = Argv::post([
            ['id',''],
            ['tab_id',''],
            ['label',''],
            ['value',''],
            ['is_check',0],
            ['remark',''],
            ['rank',1],
            ['status',1],
        ]);
        if ($data['label'] == "") return app("json")->fail("标题名称不能为空");
        if ($data['tab_id'] == "") return app("json")->fail("字典编号不能为空");
        if ($data['id'] == "")
        {
            $data['create_user'] = $this->adminId;
            $data['create_time'] = time();
            $res = dModel::insert($data);
        }else
        {
            $data['update_user'] = $this->adminId;
            $data['update_time'] = time();
            $res = dModel::update($data, ['id' => $data['id']]);
        }
        return $res ? app("json")->success("操作成功") : app("json")->fail("操作失败");
    }
}
