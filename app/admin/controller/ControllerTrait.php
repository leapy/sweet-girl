<?php


namespace app\admin\controller;

use app\Request;
use link\tools\Argv;

/**
 * Trait ModelTrait
 * @package app\admin\controller
 */
trait ControllerTrait
{
    /**
     * 删除操作
     * @param Request $request
     * @return mixed
     */
    public function del(Request $request)
    {
        $ids = $request->param("id",0);
        if (empty($ids) || !$ids) return app("json")->fail("参数有误，主键为空！");
        if (!is_array($ids)) $ids = explode(",",$ids);
        return $this->model->where($this->model->getPk(),"in",$ids)->delete() ? app("json")->success("操作成功") : app("json")->fail("操作失败");
    }

    /**
     * 软删除
     * @param Request $request
     * @return mixed
     */
    public function softDel(Request $request)
    {
        $ids = $request->param("id",0);
        if (empty($ids) || !$ids) return app("json")->fail("参数有误，主键为空！");
        if (!is_array($ids)) $ids = explode(",",$ids);
        return $this->model->where($this->model->getPk(),"in",$ids)->update(["is_del"=>1]) ? app("json")->success("操作成功") : app("json")->fail("操作失败");
    }

    /**
     * 启用
     * @param Request $request
     * @return mixed
     */
    public function enabled(Request $request)
    {
        $ids = $request->param("id",0);
        if (empty($ids) || !$ids) return app("json")->fail("参数有误，主键为空！");
        if (!is_array($ids)) $ids = explode(",",$ids);
        return $this->model->where($this->model->getPk(),"in",$ids)->update(['status'=>1,'update_time'=>time(),'update_user'=>$this->adminId]) ? app("json")->success("操作成功") : app("json")->fail("操作失败");
    }

    /**
     * 禁用
     * @param Request $request
     * @return mixed
     */
    public function disabled(Request $request)
    {
        $ids = $request->param("id",0);
        if (empty($ids) || !$ids) return app("json")->fail("参数有误，主键为空！");
        if (!is_array($ids)) $ids = explode(",",$ids);
        return $this->model->where($this->model->getPk(),"in",$ids)->update(['status'=>0,'update_time'=>time(),'update_user'=>$this->adminId]) ? app("json")->success("操作成功") : app("json")->fail("操作失败");
    }

    /**
     * 修改字段
     * @param $id
     * @return mixed
     */
    public function field($id)
    {
        if (empty($id) || !$id) return app("json")->fail("参数有误，主键为空！");
        $where = Argv::post([['field',''],['value','']]);
        if ($where['field'] === '') return app("json")->fail("参数有误！");
        return $this->model::update([$where['field']=>$where['value'],'update_time'=>time(),'update_user'=>$this->adminId],[$this->model->getPk()=>$id]) ? app("json")->success("操作成功") : app("json")->fail("操作失败");
    }
}
