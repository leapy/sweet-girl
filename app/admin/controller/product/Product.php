<?php


namespace app\admin\controller\product;


use app\admin\controller\AuthController;
use link\tools\Argv;
use app\model\product\Product as pModel;
use app\admin\validate\product\Product as pValidate;


/**
 * 商品
 * Class Product
 * @package app\admin\controller\product
 */
class Product extends AuthController
{
    /**
     * 列表
     * @return mixed
     */
    public function lst()
    {
        $where = Argv::get([
            ['name', ''],
            ['cate_id', ''],
            ['status', ''],
            ['page', 1],
            ['limit', 10],
            ['type',1],
            ['is_del',0]
        ]);
        return app("json")->success(pModel::lst($where));
    }

    /**
     * 保存
     * @return mixed
     */
    public function save()
    {
        $data = Argv::post([
            ['id',''],
            ['shop_id',1],
            ['name',''],
            ['cate_id',''],
            ['brand_id',''],
            ['unit',''],
            ['keywords',''],
            ['info',''],
            ['image',''],
            ['video',''],
            ['images',''],
            ['details',''],
            ['price',0],
            ['cost_price',0],
            ['market_price',0],
            ['is_discount',0],
            ['is_new',1],
            ['is_hot',1],
            ['postage',0],
            ['postage_model',''],
            ['sales',0],
            ['virtual_sales',0],
            ['stock',0],
            ['stock_warn',0],
            ['buy_limit',0],
            ['rank',1],
            ['integral',0],
            ['weight',0,],
            ['bulk',0],
            ['is_del',0],
            ['status',0],
            ['remark','']
        ]);
        $pValidate = new pValidate();
        if (!$pValidate->check($data)) return app("json")->fail($pValidate->getError());
        $data['details'] = htmlspecialchars($data['details']);
        return pModel::submit($data) ? app("json")->success("操作成功") : app("json")->fail("操作失败");
    }

    /**
     * 商品详情
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function detail()
    {
        $id = $this->request->param("id","");
        if (!$id) return app("json")->fail("商品ID未传！");
        $info = pModel::where("id",$id)->where("is_del",0)->find();
        if ($info)
        {
            $info = $info->toArray();
            $info['details'] = htmlspecialchars_decode($info['details']);
        }
        return $info ? app("json")->success($info) :app("json")->fail("商品不存在");
    }

    /**
     * 总数
     * @return mixed
     */
    public function counts()
    {
        return app("json")->success(pModel::productSum());
    }
}
