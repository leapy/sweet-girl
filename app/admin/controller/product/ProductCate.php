<?php


namespace app\admin\controller\product;


use app\admin\controller\AuthController;
use link\tools\Argv;
use app\model\product\ProductCate as cModel;
use app\admin\validate\product\ProductCate as cValidate;

/**
 * 商品分类
 * Class ProductCate
 * @package app\admin\controller\product
 */
class ProductCate extends AuthController
{
    /**
     * 分类列表
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function lst()
    {
        $where = Argv::get([
            ['pid', 0],
            ['cate_name', ''],
            ['status', ''],
            ['page', 1],
            ['limit', 10],
        ]);
        return app("json")->success(cModel::lst($where));
    }

    /**
     * 保存
     * @return mixed
     */
    public function save()
    {
        $data = Argv::post([
            ['id',''],
            ['pid',0],
            ['cate_name',''],
            ['image',''],
            ['remark',''],
            ['rank',0],
            ['status',1]
        ]);
        $cValidate = new cValidate();
        if (!$cValidate->check($data)) return app("json")->fail($cValidate->getError());
        return cModel::submit($data) ? app("json")->success("操作成功") : app("json")->fail("操作失败");
    }

    /**
     * 选项
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function options()
    {
        $data = cModel::where("pid",0)
            ->where("status",1)
            ->field(['id,cate_name'])
            ->order("rank desc")
            ->order("id desc")
            ->select();
        $data = $data ? $data->toArray() : [];
        array_unshift($data,['id'=>0,'cate_name'=>'顶级分类']);
        return app("json")->success($data);
    }

    /**
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function choice()
    {
        return app("json")->success(cModel::choice());
    }
}
