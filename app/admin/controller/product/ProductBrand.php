<?php
/**
 *
 * User: cfn
 * Date: 2020/10/26
 * Email: <cfn@leapy.cn>
 */

namespace app\admin\controller\product;


use app\admin\controller\AuthController;
use link\tools\Argv;
use app\model\product\ProductBrand as bModel;
use app\admin\validate\product\ProductBrand as bValidate;

/**
 * 品牌
 * Class ProductBrand
 * @package app\admin\controller\product
 */
class ProductBrand extends AuthController
{
    /**
     * 列表
     * @return mixed
     */
    public function lst()
    {
        $where = Argv::get([
            ['name', ''],
            ['status', ''],
            ['page', 1],
            ['limit', 10],
        ]);
        $condition = ['name' => 'like'];
        return app("json")->success(bModel::list($where, $condition));
    }

    /**
     * 保存
     * @return mixed
     */
    public function save()
    {
        $data = Argv::post([
            ['id',''],
            ['name',''],
            ['image',''],
            ['remark',''],
            ['rank',0],
            ['status',1]
        ]);
        $bValidate = new bValidate();
        if (!$bValidate->check($data)) return app("json")->fail($bValidate->getError());
        return bModel::submit($data) ? app("json")->success("操作成功") : app("json")->fail("操作失败");
    }

    /**
     * 选项
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function choice()
    {
        return app("json")->success(bModel::choice());
    }
}
