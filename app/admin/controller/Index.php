<?php
namespace app\admin\controller;

use app\model\admin\Admin;
use app\model\admin\AdminMenu;
use app\model\admin\AdminRole;
use link\tools\Argv;
use link\tools\Excel;
use link\tools\Http;
use link\tools\Jwt;
use link\tools\Rsa;
use Swoole\Client;
use think\Db;
use think\facade\Cache;

/**
 * 后台首页
 * Class Index
 * @package app\admin\controller
 */
class Index extends AuthController
{
    /**
     * 无需登录的方法
     * @var string[]
     */
    protected $noNeedLogin = ['login','test'];

    /**
     * 测试
     */
    public function test()
    {
//        $channel = new \Swoole\Coroutine\Channel();
//        $channel->push([]);
//        $data = Db::name("show tables");
//        Excel::export("导出测试样例",['A','B','C'],[[1,2,3],[4,5,6],[7,8,9]],"共三条数据");
//        $data = Excel::import($this->app->getRootPath() . "public/upload/1.xlsx");
//        print_r($data);
//        var_dump(SWOOLE_VERSION);
//        $client = new \Swoole\Client(SWOOLE_SOCK_TCP);
//        if (!$client->connect('127.0.0.1', 6789, -1)) {
//            exit("connect failed. Error: {$client->errCode}\n");
//        }
//        $i = 30;
//        while ($i > 0)
//        {
//            $i--;
//            $client->send("hello world\n");
//        }
//        $client->close();

//        $client = new \link\swoole\Client();
//        $client->send(json_encode(['event'=>'test','data'=>['code'=>0]]));
//        $rsa = new Rsa();
//        $rsa->keyPair();
//        $e = event('swoole.websocket.SendAll', ['message' => "ok"]);
//        var_dump($e);
//        $res = Http::post("http://localhost:6789/index/index/test",['message' => "1234567890"],[],30);
//        var_dump($res);
        $client = new \Swoole\Client(SWOOLE_SOCK_TCP);
        if (!$client->connect('127.0.0.1', 6789, -1)) {
            exit("connect failed. Error: {$client->errCode}\n");
        }
        $client->send("hello world\n");
//        echo $client->recv();
        $client->close();
    }

    /**
     * 无需权限
     * @return mixed
     * @throws \Exception
     */
    public function index()
    {
        return app("json")->success("ok");
    }

    /**
     * 测试权限
     */
    public function auth()
    {
        return app("json")->success("恭喜您！".$this->adminInfo['nickname'].", 访问成功。");
    }

    /**
     * 管理用户登录
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function login()
    {
        $params = Argv::post([
            'username',
            'password'
        ]);
        if (empty($params['username']) || empty($params['password'])) return app("json")->fail("用户和密码必填！");
        $adminInfo = Admin::where('name|tel|email',$params['username'])->find();
        if (!empty($adminInfo))
        {
            $adminInfo = $adminInfo->toArray();
            if (password_verify($params['password'],$adminInfo['password']))
            {
                event("AdminLoginBefore",['id'=>$adminInfo['id']]);
                $token = Jwt::createToken($adminInfo);
                Cache::store('redis')->set(md5($adminInfo['id']),$token,3600 * 24);
                return app("json")->success("登录成功！",compact('token'));
            }
            return app("json")->fail("用户名或密码错误！");
        }
        return app("json")->fail("用户不存在！");
    }

    /**
     * 退出登录
     * @return mixed
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function logout()
    {
        // 清理掉token
        Cache::store('redis')->delete(md5($this->adminId));
        return app("json")->success("退出成功");
    }

    /**
     * 获取最新的用户信息
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function info()
    {
        $data['info'] = Admin::getAdminInfo($this->adminId)->toArray();
        $data['avatar'] = $data['info']['avatar'];
        $data['email'] = $data['info']['email'];
        $data['name'] = $data['info']['name'];
        $data['info']['role_name'] = AdminRole::getNameById($data['info']['role_id']);
        $data['roles'] = ['admin'];
        $data['permissions'] = ["*:*:*"];
        $data['menu'] = AdminMenu::getTreeMenu($this->menu);
        return app("json")->success($data);
    }
}
