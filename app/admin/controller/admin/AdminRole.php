<?php


namespace app\admin\controller\admin;

use app\admin\controller\AuthController;
use app\model\admin\AdminMenu as mModel;
use app\model\admin\AdminAuth as aModel;
use link\tools\Argv;
use app\model\admin\AdminRole as rModel;

/**
 * 角色
 * Class AdminRole
 * @package app\admin\controller\admin
 */
class AdminRole extends AuthController
{
    /**
     * 角色列表
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index()
    {
        $where = Argv::get([
            ['name',''],
            ['pid',0],
            ['status',''],
        ]);
        return app('json')->success(rModel::roles($where['pid'], $this->auth, $where));
    }

    /**
     * 保存
     * @return mixed
     */
    public function submit()
    {
        $data = Argv::post([
            ['id',''],
            ['name',''],
            ['pid',0],
            ['auth',''],
            ['menu',''],
            ['remark',''],
            ['rank',0],
            ['status',1]
        ]);
        if ($data['name'] == "") return app("json")->fail("角色名称不能为空");
        if ($data['pid'] === "") return app("json")->fail("上级归属不能为空");
        if ($data['id'] != "")
        {
            $data['update_user'] = $this->adminId;
            $data['update_time'] = time();
            $res = rModel::update($data,['id'=>$data['id']]);
        }else
        {
            $data['create_user'] = $this->adminId;
            $data['create_time'] = time();
            $res = rModel::insert($data);
        }
        return $res ? app("json")->success("操作成功") : app("json")->fail("操作失败");
    }

    /**
     * 下拉菜单
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function selectOptions()
    {
        return app('json')->success(rModel::returnOptions($this->adminInfo['role_id']));
    }

    /**
     * 权限组
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function authOptions()
    {
        return app('json')->success(aModel::lst(0, $this->auth));
    }

    /**
     * 权限组
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function menuOptions()
    {
        return app('json')->success(mModel::lst(0, $this->menu));
    }
}
