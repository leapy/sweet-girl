<?php
/**
 *
 * User: cfn
 * Date: 2020/10/14
 * Email: <cfn@leapy.cn>
 */

namespace app\admin\controller\admin;


use app\admin\controller\AuthController;
use app\model\admin\AdminLog as lModel;
use link\tools\Argv;

/**
 * Class AdminLog
 * @package app\admin\controller\admin
 */
class AdminLog extends AuthController
{
    /**
     * 操作日志列表
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function lst()
    {
        $where = Argv::get([
            ['admin_id',$this->adminId],
            ['page', 1],
            ['limit', 10],
        ]);
        return app("json")->success(lModel::lst($where));
    }
}
