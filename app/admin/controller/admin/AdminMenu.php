<?php


namespace app\admin\controller\admin;

use app\admin\controller\AuthController;
use app\model\admin\AdminMenu as mModel;
use app\model\admin\AdminRole as rModel;
use link\exception\ModelException;
use link\tools\Argv;
use app\admin\validate\admin\AdminMenu as aValidate;

/**
 * 权限控制
 * Class AdminAuth
 * @package app\admin\controller\admin
 */
class AdminMenu extends AuthController
{
    /**
     * 权限列表
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index()
    {
        $where = Argv::get([
            ['pid',0],
            ['title',''],
            ['name',''],
            ['is_category',''],
            ['is_show',''],
            ['status',''],
        ]);
        return app('json')->success(mModel::allMenus($where['pid'], $this->menu, $where));
    }

    /**
     * 添加
     * @return mixed
     */
    public function submit()
    {
        $data = Argv::post([
            ['id',''],
            ['title',''],
            ['icon',''],
            ['pid',0],
            ['params',''],
            ['is_menu',1],
            ['is_show',1],
            ['is_category',1],
            ['name',''],
            ['path',''],
            ['component',''],
            ['rank',1],
            ['status',1],
            ['remark',''],
        ]);
        $validate = new aValidate();
        $result = $validate->check($data);
        if(!$result){
            return app("json")->fail($validate->getError());
        }
        if ($data['id'] != "")
        {
            $data['update_user'] = $this->adminId;
            $data['update_time'] = time();
            return mModel::where("id",$data['id'])->update($data) ? app("json")->success("修改成功") : app("json")->fail("修改失败");
        }else
        {
            $data['create_user'] = $this->adminId;
            $data['create_time'] = time();

            rModel::startTrans();
            // 超级管理员追加菜单
            // 当前角色追加菜单
            try {
                $mid = mModel::insertGetId($data);
                rModel::appendValue(1,"menu", $mid, ",");
                rModel::appendValue($this->adminInfo['role_id'],"menu", $mid, ",");
                rModel::commit();
                return app("json")->success("添加成功");
            }catch (ModelException $e)
            {
                rModel::rollback();
                return app("json")->fail($e->getErrorMessage());
            }
        }
    }

    /**
     * 下拉菜单
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function selectOptions()
    {
        return app('json')->success(mModel::returnOptions(0, $this->menu));
    }
}
