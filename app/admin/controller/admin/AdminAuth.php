<?php


namespace app\admin\controller\admin;


use app\admin\controller\AuthController;
use link\exception\ModelException;
use link\tools\Argv;
use app\model\admin\AdminAuth as aModel;
use app\admin\validate\admin\AdminAuth as aValidate;
use app\model\admin\AdminRole as rModel;

/**
 * Class AdminAuth
 * @package app\admin\controller\admin
 */
class AdminAuth extends AuthController
{
    /**
     * 权限列表
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index()
    {
        $where = Argv::get([
            ['pid',0],
            ['name',''],
            ['is_category',''],
            ['is_show',''],
            ['status',''],
        ]);
        return app('json')->success(aModel::allMenus($where['pid'], $this->auth, $where));
    }

    /**
     * 添加
     * @return mixed
     */
    public function submit()
    {
        $data = Argv::post([
            ['id',''],
            ['name',''],
            ['pid',0],
            ['is_category',1],
            ['controller',''],
            ['module',''],
            ['action',''],
            ['rank',1],
            ['status',1],
            ['remark',''],
        ]);
        $validate = new aValidate();
        $result = $validate->check($data);
        if(!$result){
            return app("json")->fail($validate->getError());
        }
        if ($data['id'] != "")
        {
            $data['update_user'] = $this->adminId;
            $data['update_time'] = time();
            return aModel::where("id",$data['id'])->update($data) ? app("json")->success("修改成功") : app("json")->fail("修改失败");
        }else
        {
            $data['create_user'] = $this->adminId;
            $data['create_time'] = time();
            aModel::startTrans();
            // 超级管理员追加权限
            // 当前角色追加权限
            try {
                $rid = aModel::insertGetId($data);
                rModel::appendValue(1,"auth", $rid, ",");
                rModel::appendValue($this->adminInfo['role_id'],"auth", $rid, ",");
                aModel::commit();
                return app("json")->success("添加成功");
            }catch (ModelException $e)
            {
                aModel::rollback();
                return app("json")->fail($e->getErrorMessage());
            }
        }
    }

    /**
     * 下拉菜单
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function selectOptions()
    {
        return app('json')->success(aModel::returnOptions(0, $this->menu));
    }
}
