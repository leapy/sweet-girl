<?php


namespace app\admin\controller\admin;

use app\admin\controller\AuthController;
use link\exception\MailException;
use link\exception\SmsException;
use link\tools\Argv;
use app\model\admin\Admin as aModel;
use link\tools\Mail;
use link\tools\Sms;
use think\facade\Cache;

/**
 * 用户
 * Class AdminUser
 * @package app\admin\controller\admin
 */
class Admin extends AuthController
{
    /**
     * 角色列表
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index()
    {
        $where = Argv::get([
            ['name',''],
            ['role_id',''],
            ['tel',''],
            ['email',''],
            ['uid',''],
            ['status',''],
            ['page',1],
            ['limit',10],
        ]);
        return app('json')->success(aModel::lst($where));
    }

    /**
     * 保存
     * @return mixed
     * @throws \Exception
     */
    public function save()
    {
        $data = Argv::post([
            ['id',''],
            ['name',''],
            ['role_id',0],
            ['password',''],
            ['tel',''],
            ['email',''],
            ['status',1]
        ]);
        if ($data['name'] == "") return app("json")->fail("名称不能为空");
        if ($data['role_id'] === "") return app("json")->fail("上级归属不能为空");
        if ($data['id'] != "")
        {
            if ($data['password']) $data['password'] = password_hash($data['password'],PASSWORD_DEFAULT);
            else unset($data['password']);
            $res = aModel::update($data,['id'=>$data['id']]);
        }else
        {
            if (aModel::issetValue("name", $data['name'])) return app("json")->fail("名称已存在");
            $data['nickname'] = waterNo(new aModel());
            $data['avatar'] = "https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif";
            $data['password'] = password_hash($data['password'],PASSWORD_DEFAULT);
            $data['integral'] = 0;
            $data['money'] = 0;
            $data['expire_time'] = date("Y-m-d H:i:s");
            $data['add_time'] = date("Y-m-d H:i:s");
            $data['add_ip'] = $this->request->ip();
            $data['count'] = 0;
            $res = aModel::insert($data);
        }
        return $res ? app("json")->success("操作成功") : app("json")->fail("操作失败");
    }

    /**
     * 修改密码
     * @return mixed
     */
    public function changePassword()
    {
        $data = Argv::post([
            ['old_password',''],
            ['new_password','']
        ]);
        if (empty($data['old_password']) || empty($data['new_password'])) return app("json")->fail("新旧密码不能为空");
        if (!password_verify($data['old_password'],$this->adminInfo['password'])) return app("json")->fail("原密码不正确");
        return aModel::where("id",$this->adminId)->update(['password'=>password_hash($data['new_password'],PASSWORD_DEFAULT)]) ? app("json")->success("操作成功") : app("json")->fail("操作失败");
    }

    /**
     * 基本信息查看
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function baseInfo()
    {
        $data = aModel::where("id",$this->adminId)->field(['nickname,name,avatar,remark'])->find();
        if ($data) $data = $data->toArray() ?: [];
        return app("json")->success($data);
    }

    /**
     * 保存基本信息
     * @return mixed
     */
    public function saveBaseInfo()
    {
        $data = Argv::post([
            ['nickname',''],
            ['name',''],
            ['avatar',''],
            ['remark','']
        ]);
        if (aModel::where("id",$this->adminId)->value("name") == $data['name']) unset($data['name']);
        if (isset($data['name']) && aModel::issetValue("name",$data['name'])) return app("json")->success("登录账户名重复，修改失败！");
        return aModel::where("id",$this->adminId)->update($data) ? app("json")->success("操作成功") : app("json")->fail("操作失败");
    }

    /**
     * 获取手机号邮箱信息
     * @return mixed
     */
    public function securityInfo()
    {
        $data['email'] = aModel::where("id",$this->adminId)->value("email");
        $data['tel'] = aModel::where("id",$this->adminId)->value("tel");
        // 隐藏手机号中间四位
        $data['tel'] = substr_replace($data['tel'], '****', 3, 4);
        // 隐藏邮箱
        $arr = explode("@",$data['email']);
        $data['email'] = substr($arr[0], 0, 1)."***"."@".$arr[1];
        return app("json")->success($data);
    }

    /**
     * 修改手机号邮箱
     * @return mixed
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function modify()
    {
        $data = Argv::post([
            ['tel',''],
            ['email',''],
            ['code','']
        ]);
        if (empty($data['code'])) return app("json")->error("验证码未填写！");
        if (!empty($data['tel']))
        {
            if (!checkMobileFormat($data['tel'])) return app("json")->error("手机号格式错误！");
            if ($data['tel'] == aModel::where("id",$this->adminId)->value("tel")) return app("json")->error("手机号一致，无需修改！");
            if (!Cache::store("redis")->has("modify_tel_".$this->adminId)) return app("json")->error("验证码已过期！");
            if (md5(trim($data['code'])) != md5(Cache::store("redis")->get("modify_tel_".$this->adminId))) return app("json")->error("验证码不正确！");
            return aModel::where("id",$this->adminId)->update(['tel'=>$data['tel']]) ? app("json")->success("修改成功") : app("json")->error("修改失败");
        }
        if (!empty($data['email']))
        {
            if (!checkEmailFormat($data['email'])) return app("json")->error("邮箱格式错误！");
            if ($data['email'] == aModel::where("id",$this->adminId)->value("email")) return app("json")->error("邮箱一致，无需修改！");
            if (!Cache::store("redis")->has("modify_email_".$this->adminId)) return app("json")->error("验证码已过期！");
            if (md5(trim($data['code'])) != md5(Cache::store("redis")->get("modify_email_".$this->adminId))) return app("json")->error("验证码不正确！");
            return aModel::where("id",$this->adminId)->update(['email'=>$data['email']]) ? app("json")->success("修改成功") : app("json")->error("修改失败");
        }
    }

    /**
     * 发送验证码
     * @return mixed
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function verify()
    {
        $data = Argv::post([
            ['tel',''],
            ['email',''],
        ]);

        if (!empty($data['tel']))
        {
            if (!checkMobileFormat($data['tel'])) return app("json")->error("手机号格式错误！");
            if ($data['tel'] == aModel::where("id",$this->adminId)->value("tel")) return app("json")->error("手机号一致，无需修改！");
            try {
                $code = rand(1000,9999);
                $res = Sms::modifyCode($data['tel'], $code);
                Cache::store("redis")->set("modify_tel_".$this->adminId,$code,300);
                return $res ? app("json")->success("发送成功") : app("json")->error("发送失败, 未知错误！");
            }catch (SmsException $e)
            {
                return app("json")->fail($e->getErrorMessage());
            }catch (\Exception $e)
            {
                return app("json")->fail($e->getMessage());
            }
        }

        if (!empty($data['email']))
        {
            if (!checkEmailFormat($data['email'])) return app("json")->error("邮箱格式错误！");
            if ($data['email'] == aModel::where("id",$this->adminId)->value("email")) return app("json")->error("邮箱一致，无需修改！");
            try {
                $code = rand(1000,9999);
                $res = Mail::modifyCode($data['email'], $code);
                Cache::store("redis")->set("modify_email_".$this->adminId,$code,300);
                return $res ? app("json")->success("发送成功") : app("json")->error("发送失败, 未知错误！");
            }catch (MailException $e)
            {
                return app("json")->fail($e->getErrorMessage());
            }catch (\Exception $e)
            {
                return app("json")->fail($e->getMessage());
            }
        }

        return app("json")->fail("错误的操作类型");
    }
}
