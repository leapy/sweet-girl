<?php


namespace app\admin\controller\common;


use app\admin\controller\AuthController;
use link\exception\StorageException;
use link\tools\Argv;
use link\tools\Storage;
use app\model\file\FileTab as ftModel;
use app\model\file\File as fModel;

/**
 * 文件管理
 * Class File
 * @package app\admin\controller\common
 */
class File extends AuthController
{
    /**
     * 上传图片
     * @return mixed
     */
    public function uploadImageOne()
    {
        try {
            $data = Argv::post([
                ['tab_id',0],
                ['type','image']
            ]);
            $file = $this->request->file("file");
            $saveName = Storage::put($file);
            $data['name'] = $file->getOriginalName();
            $data['path'] = $saveName;
            $data['size'] = $file->getSize();
            $data['mime'] = $file->getOriginalMime();
            $data['storage'] = systemConfig("storage_type");
            $data['add_time'] = time();
            $res = fModel::insert($data);
            return $saveName && $res ? app("json")->success("上传成功",['path'=>$saveName]) : app("json")->error("上传失败");
        }catch (StorageException $e)
        {
            return app("json")->error($e->getErrorMessage());
        }catch (\Exception $e)
        {
            return app("json")->error($e->getMessage());
        }
    }

    /**
     * 删除
     * @return mixed
     */
    public function delImage()
    {
        try {
            $files = $this->request->param("file");
            foreach ($files as $file)
            {
                fModel::where("path",$file)->delete();
                $arr = explode(systemConfig("asset_domain"), $file);
                Storage::del($file, count($arr) > 1 ? 1 : 2);
            }
            return app("json")->success("删除成功");
        }catch (StorageException $e)
        {
            return app("json")->error($e->getErrorMessage());
        }catch (\Exception $e)
        {
            return app("json")->error($e->getMessage());
        }
    }

    /**
     * 保存
     * @return mixed
     */
    public function saveMenu()
    {
        $data = Argv::post([
            ['pid',0],
            ['id',''],
            ['name',''],
            ['type','image'],
            ['status',1],
            ['rank',1]
        ]);
        if ($data['id'] != "")
        {
            $data['update_user'] = $this->adminId;
            $data['update_time'] = time();
            $res = ftModel::update($data,['id'=>$data['id']]);
        }else
        {
            $data['create_user'] = $this->adminId;
            $data['create_time'] = time();
            $res = ftModel::insert($data);
        }
        return $res ? app("json")->success("操作成功") : app("json")->error("操作失败");
    }

    /**
     * 列表
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function menuLst()
    {
        return app("json")->success(ftModel::lst(0));
    }

    /**
     * 删除分类
     * @return mixed
     */
    public function delMenu()
    {
        return ftModel::where("id",$this->request->param("id"))->delete() ? app("json")->success("删除成功") : app("json")->error("删除失败");
    }

    /**
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function menuOption()
    {
        return app("json")->success(ftModel::returnOptions(0));
    }

    /**
     * 文件列表
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function fileList()
    {
        $where = Argv::get([
            ['page',1],
            ['limit',30],
            ['type','image'],
            ['tab_id',''],
        ]);
        return app("json")->success(fModel::dataRows($where));
    }
}
