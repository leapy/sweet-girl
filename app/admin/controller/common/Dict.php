<?php


namespace app\admin\controller\common;


use app\admin\controller\AuthController;
use app\model\data\Dict as dModel;

/**
 * Class Dict
 * @package app\admin\controller\common
 */
class Dict extends AuthController
{
    /**
     * 无效授权的接口
     * @var string[]
     */
    protected $noNeedRight = ['dict'];

    /**
     * 从字典获取数据选项
     * 选项和默认选中值
     * @param $tab_id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function dict($tab_id)
    {
        $option = dModel::where("status",1)
            ->where("tab_id", $tab_id)
            ->order("rank desc")
            ->field(['label','value'])
            ->select();
        $check = dModel::where("status",1)
            ->where("is_check", 1)
            ->where("tab_id", $tab_id)
            ->column("value");
        if ($option) $option = $option->toArray();
        return app("json")->success(compact("option","check"));
    }
}
