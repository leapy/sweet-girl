<?php


namespace app\admin\controller\system;

use app\admin\controller\AuthController;
use link\tools\Argv;
use app\model\system\SystemConfigTab as tModel;

/**
 * 系统配置菜单
 * Class SyatemConfigTab
 * @package app\admin\controller\system
 */
class SystemConfigTab extends AuthController
{
    /**
     * 列表
     * @return array
     */
    public function lst()
    {
        $where = Argv::get([
            ['id', ''],
            ['name', ''],
            ['status', ''],
            ['page', 1],
            ['limit', 10],
        ]);
        $condition = ['name' => 'like'];
        return app("json")->success(tModel::list($where, $condition));
    }

    /**
     * 保存修改
     * @return mixed
     */
    public function save()
    {
        $data = Argv::post([
            ['id',''],
            ['name',''],
            ['rank',1],
            ['status',1]
        ]);
        if ($data['name'] == "") return app("json")->fail("分类名称不能为空");
        if ($data['id'] == "")
        {
            $data['create_user'] = $this->adminId;
            $data['create_time'] = time();
            $res = tModel::insert($data);
        }else
        {
            $data['update_user'] = $this->adminId;
            $data['update_time'] = time();
            $res = tModel::update($data, ['id' => $data['id']]);
        }
        return $res ? app("json")->success("操作成功") : app("json")->fail("操作失败");
    }

    /**
     * 获取系统配置菜单
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function options()
    {
        $options = tModel::where("status",1)->field(["id","name"])->select();
        return app("json")->success($options ? $options->toArray() : []);
    }
}
