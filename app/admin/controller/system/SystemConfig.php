<?php


namespace app\admin\controller\system;

use app\admin\controller\AuthController;
use app\model\system\SystemConfig as cModel;
use link\exception\MailException;
use link\exception\SmsException;
use link\tools\Argv;
use link\tools\Mail;
use link\tools\Sms;

/**
 * 系统配置
 * Class SystemConfig
 * @package app\admin\controller\system
 */
class SystemConfig extends AuthController
{
    /**
     * 列表
     * @return array
     */
    public function lst()
    {
        $where = Argv::get([
            ['tab_id', ''],
            ['name', ''],
            ['form_name', ''],
            ['status', ''],
            ['page', 1],
            ['limit', 10],
        ]);
        $condition = ['name' => 'like'];
        return app("json")->success(cModel::list($where, $condition));
    }

    /**
     * 保存修改
     * @return mixed
     */
    public function save()
    {
        $data = Argv::post([
            ['id',''],
            ['tab_id',''],
            ['form_name',''],
            ['form_type',''],
            ['tag_type',''],
            ['upload_type',''],
            ['param',''],
            ['value',''],
            ['remark',''],
            ['is_show',''],
            ['name',''],
            ['rank',1],
            ['status',1]
        ]);
        if ($data['name'] == "") return app("json")->fail("分类名称不能为空");
        if ($data['id'] == "")
        {
            $data['create_user'] = $this->adminId;
            $data['create_time'] = time();
            $res = cModel::insert($data);
        }else
        {
            $data['update_user'] = $this->adminId;
            $data['update_time'] = time();
            $res = cModel::update($data, ['id' => $data['id']]);
        }
        return $res ? app("json")->success("操作成功") : app("json")->fail("操作失败");
    }

    /**
     * 下拉框
     * @return mixed
     */
    public function options()
    {
        $data['tags'] = tagOptions();
        $data['types'] = typeOptions();
        return app("json")->success($data);
    }

    /**
     * 获取所有配置项
     * @param $tab_id
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function items($tab_id)
    {
        // 获取某项配置菜单下的所有配置项
        $data = cModel::where("status",1)
            ->where("tab_id", $tab_id)
            ->where("is_show",1)
            ->order("rank desc")
            ->select()
            ->each(function ($item){
                if ($item['form_type'] == "number") $item['value'] = (int)$item['value'];
                if ($item['form_type'] == "radio" && !empty($item['param']))
                {
                    $tmp = explode("\n", $item['param']);
                    foreach ($tmp as $k => $v) $tmp[$k] = explode("=>", $v);
                    $item['param'] = $tmp;
                }
            });
        return $data ? app("json")->success($data->toArray()) : app("json")->error("配置项为空");
    }

    /**
     * 保存配置项
     * @return mixed
     */
    public function saveAll()
    {
        $data = $this->request->param();
        foreach ($data as $k => $v) cModel::where("form_name", $k)->update(['value'=>trim($v)]);
        return app("json")->success("操作成功");
    }

    /**
     * 测试发送短信
     */
    public function sendSms()
    {
        try {
            $code = rand(1000,9999);
            $res = Sms::loginCode($this->request->param("mobile"), $code);
            return $res ? app("json")->success("发送成功",["code"=>$code]) : app("json")->error("发送失败, 未知错误！");
        }catch (SmsException $e)
        {
            return app("json")->fail($e->getErrorMessage());
        }catch (\Exception $e)
        {
            return app("json")->fail($e->getMessage());
        }
    }

    /**
     * 保存邮件模板
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function saveEmailTemp()
    {
        $data = Argv::post([
            ['form_name', ''],
            ['value', '']
        ]);
        // 获取某项配置菜单下的所有配置项
        if (empty($data['form_name']) || empty($data['value'])) return app("json")->error("数据为空");
        return cModel::where("form_name", $data['form_name'])->update(['value'=>htmlspecialchars($data['value']),'update_user'=>$this->adminId,'update_time'=>time()]) ? app("json")->success("修改成功") : app("json")->error("修改失败");
    }

    /**
     * 邮件模板
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function emailTemp()
    {
        // 获取某项配置菜单下的所有配置项
        $data = cModel::where("status",1)
            ->where("form_name",'in', ['mail_login','mail_register','mail_retrieve','mail_modify'])
            ->order("rank desc")
            ->select()
            ->each(function ($item){
                $item['value'] = htmlspecialchars_decode($item['value']);
            });
        return $data ? app("json")->success($data->toArray()) : app("json")->error("配置项为空");
    }

    /**
     * 测试发送邮件
     */
    public function sendEmail()
    {
        try {
            $code = rand(1000,9999);
            $res = Mail::loginCode($this->request->param("to"), $code);
            return $res ? app("json")->success("发送成功",["code"=>$code]) : app("json")->error("发送失败, 未知错误！");
        }catch (MailException $e)
        {
            return app("json")->fail($e->getErrorMessage());
        }catch (\Exception $e)
        {
            return app("json")->fail($e->getMessage());
        }
    }
}
