<?php

if (!function_exists('adminId'))
{
    /**
     * 管理员id
     * @return int
     */
    function adminId(): int
    {
        return app('request')->adminId();
    }
}

if (!function_exists('adminInfo'))
{
    /**
     * 管理员信息
     * @return array
     */
    function adminInfo(): array
    {
        return app("request")->adminInfo();
    }
}

if (!function_exists('tagOptions'))
{
    /**
     * 获取form标签
     * @return array
     */
    function tagOptions(): array
    {
        $menus[] = ['value'=>"input",'label'=>"input"];
        $menus[] = ['value'=>"textarea",'label'=>"textarea"];
        $menus[] = ['value'=>"select",'label'=>"select"];
        return $menus;
    }
}

if (!function_exists('typeOptions'))
{
    /**
     * 获取form标签
     * @return array
     */
    function typeOptions(): array
    {
        $menus[] = ['value'=>"text",'label'=>"text"];
        $menus[] = ['value'=>"radio",'label'=>"radio"];
        $menus[] = ['value'=>"password",'label'=>"password"];
        $menus[] = ['value'=>"checkbox",'label'=>"checkbox"];
        $menus[] = ['value'=>"number",'label'=>"number"];
        $menus[] = ['value'=>"hidden",'label'=>"hidden"];
        $menus[] = ['value'=>"email",'label'=>"email"];
        $menus[] = ['value'=>"tel",'label'=>"tel"];
        $menus[] = ['value'=>"date",'label'=>"date"];
        $menus[] = ['value'=>"file",'label'=>"file"];
        return $menus;
    }
}

if (!function_exists("checkMobileFormat"))
{
    /**
     * 验证手机号格式
     * @param $sMobile
     * @return false|int
     */
    function checkMobileFormat($sMobile)
    {
        $sPregMatch = '/^1(3[0-9]|4[57]|5[0-35-9]|7[0-9]|8[0-9])\d{8}$/';
        return preg_match($sPregMatch, $sMobile);
    }
}

if (!function_exists("checkEmailFormat"))
{
    /**
     * 验证手机号格式
     * @param $email
     * @return false|int
     */
    function checkEmailFormat($email)
    {
        $pattern = "/^[^_][\w]*@[\w.]+[\w]*[^_]$/";
        if(preg_match($pattern, $email, $matches)){
            return true;
        }
        return false;
    }
}
