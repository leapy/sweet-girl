<?php
// 应用公共文件

if (!function_exists("waterNo"))
{
    /**
     * 获取流水号
     * @param $model
     * @param int $len
     * @param string $prefix
     * @return string
     */
    function waterNo($model, $len = 9, $prefix = "U")
    {
        return $prefix . sprintf("%0{$len}d", $model->count()+1);
    }
}

if (!function_exists('unCamelize'))
{
    /**
     * 驼峰法转下划线
     * @param $camelCaps
     * @param string $separator
     * @return string
     */
    function unCamelize($camelCaps,$separator='_')
    {
        return strtolower(preg_replace('/([a-z])([A-Z])/', "$1" . $separator . "$2", $camelCaps));
    }
}

if (!function_exists("systemConfig"))
{
    /**
     * 获取系统配置
     * @param $names
     * @return mixed|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    function systemConfig($names)
    {
        return \app\model\system\SystemConfig::getValueByNames($names);
    }
}
