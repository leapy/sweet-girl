<?php
declare (strict_types = 1);

namespace app;

use link\tools\Packet;
use link\tools\Json;
use think\Service;

/**
 * 应用服务类
 */
class AppService extends Service
{

    public $bind = [
        'json' => Json::class,
        'packet' => Packet::class
    ];

    public function register()
    {
        // 服务注册
    }

    public function boot()
    {
        // 服务启动
    }
}
