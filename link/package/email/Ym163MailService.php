<?php


namespace link\package\email;

use link\exception\MailException;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/**
 * 网易域名邮箱服务
 * Class Ym163MailService
 * @package link\package\email
 */
class Ym163MailService
{
    /**
     * 调试模式
     * @var int
     */
    protected $debug = 0;

    /**
     * 编码格式
     * @var string
     */
    protected $charSet = "UTF-8";

    /**
     * 服务
     * @var
     */
    protected $host = "smtp.ym.163.com";

    /**
     * 端口
     * @var int
     */
    protected $port = 25;

    /**
     * 账号
     * @var
     */
    protected $username;

    /**
     * 密码
     * @var
     */
    protected $password;

    /**
     * 签名
     * @var
     */
    protected $sign;

    /**
     * 服务
     * @var null
     */
    protected $email=null;

    /**
     * Ym163MailService constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        if (isset($config['mail_host'])) $this->host = $config['mail_host'];
        if (isset($config['mail_port'])) $this->port = $config['mail_port'];
        $this->password = $config['mail_password'];
        $this->username = $config['mail_username'];
        $this->sign = $config['mail_from_name'];
        if (isset($config['charSet'])) $this->charSet = $config['charSet'];
        if (isset($config['debug'])) $this->debug = $config['debug'];
        $this->instance();
    }

    /**
     * 初始化
     */
    public function instance()
    {
        try {
            $this->email = new PHPMailer(true);
            $this->email->Host = $this->host;
            $this->email->Port = $this->port;
            $this->email->Username = $this->username;
            $this->email->Password = $this->password;
            $this->email->CharSet = $this->charSet;
            $this->email->SMTPDebug = $this->debug;
            $this->email->setFrom($this->username,$this->sign);
            $this->email->isSMTP();
            $this->email->SMTPAuth = true;
            $this->email->SMTPSecure = 'ssl';
        }catch (Exception $e)
        {
            throw new MailException($e->errorMessage());
        }
    }

    /**
     * 邮件类型
     * @param string $subtype
     * @param string $altBody
     * @return Ym163MailService
     */
    public function subtype(string $subtype = "plain", string $altBody = "邮件客户端不支持HTML")
    {
        if ($subtype == "html")  $this->email->isHTML(true);
        $this->email->AltBody = $altBody;
        return $this;
    }

    /**
     * 接收者
     * @param string $to
     * @return null
     */
    public function to(string $to)
    {
        $this->email->addAddress($to);
        return $this;
    }

    /**
     * @param string $subject
     * @param string $body
     * @return bool
     */
    public function send(string $subject, string $body)
    {
        try {
            $this->email->Subject = $subject;
            $this->email->Body    = $body;
            $this->email->send();
            return true;
        }catch (Exception $e)
        {
            throw new MailException($e->errorMessage());
        }
    }
}
