<?php


namespace link\package\storage;

use link\exception\StorageException;
use Qcloud\Cos\Client;

/**
 * 腾讯云COS存储
 * Class QcloudCoService
 * @package learn\services\storage
 */
class QCloudCosService
{
    /**
     * @var null
     */
    protected $cos = null;

    /**
     * 设置存储位置
     * @var null
     */
    protected $bucket = null;

    /**
     * QCloudCosService constructor.
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function __construct()
    {
        $this->cos = new Client(self::options());
        return $this;
    }

    /**
     * 配置
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function options()
    {
        $config = systemConfig(['storage_secretid','storage_secretkey','storage_region','storage_bucket']);
        return [
            'region' => $config['storage_region'],
            'credentials' => [
                'secretId'  => $config['storage_secretid'],
                'secretKey' => $config['storage_secretkey']
            ]
        ];
    }

    /**
     * 上传指定资源
     * @param string $name
     * @param string $filePath
     * @return string
     */
    public function put(string $name, string $filePath)
    {
        try {
            $result = $this->cos->putObject([
                'Bucket' => systemConfig("storage_bucket"),
                'Key' => $name,
                'Body' => file_get_contents($filePath)
            ]);
            return $result ? systemConfig("storage_domain").$name : false;
        }
        catch (\Exception $e)
        {
            throw new StorageException($e->getMessage());
        }
    }

    /**
     * 删除指定资源
     * @param string $name
     * @return string
     */
    public function del(string $name)
    {
        try {
            return $this->cos->deleteObject([
                'Bucket' => systemConfig("storage_bucket"),
                'Key' => str_replace(systemConfig("storage_domain"),"", $name),
            ]);
        }catch (\Exception $e)
        {
            throw new StorageException($e->getMessage());
        }
    }
}
