<?php
/**
 *
 * User: cfn
 * Date: 2020/10/14
 * Email: <cfn@leapy.cn>
 */

namespace link\package\http;

/**
 * Class Curl
 * @package link\package\http
 */
class Curl
{
    /**
     * @var string
     */
    private $url = "";

    /**
     * @var array
     */
    private $data = array();

    /**
     * @var array
     */
    private $header = array("Content-type:application/json;charset='utf-8'");

    /**
     * 超时时间
     * @var int
     */
    private $timeout = 30;

    /**
     * Curl constructor.
     * @param string $url
     * @param array $data
     * @param array $header
     * @param int $timeout
     */
    public function __construct(string $url, array $data = [], array $header = [], int $timeout)
    {
        $this->url = $url;
        if (!empty($data)) $this->data = $data;
        if (!empty($header)) $this->header = $header;
        if ($timeout) $this->timeout = $timeout;
    }

    /**
     * @return mixed
     */
    public function get()
    {
        $curl = curl_init();
        curl_setopt($curl,CURLOPT_URL, $this->url);
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,FALSE);
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,FALSE);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($curl,CURLOPT_HTTPHEADER, $this->header);
        curl_setopt($curl,CURLOPT_TIMEOUT, $this->timeout);
        $output = curl_exec($curl);
        curl_close($curl);
        return json_decode($output,true);
    }

    /**
     * @return mixed
     */
    public function post()
    {
        $curl = curl_init();
        curl_setopt($curl,CURLOPT_URL, $this->url);
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,FALSE);
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,FALSE);
        curl_setopt($curl,CURLOPT_POST, 1);
        curl_setopt($curl,CURLOPT_POSTFIELDS,$this->data);
        curl_setopt($curl,CURLOPT_HTTPHEADER,$this->header);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl,CURLOPT_TIMEOUT, $this->timeout);
        $output = curl_exec($curl);
        curl_close($curl);
        return json_decode($output,true);
    }

    /**
     * @return mixed
     */
    public function put()
    {
        $curl = curl_init();
        curl_setopt($curl,CURLOPT_URL, $this->url);
        curl_setopt($curl,CURLOPT_HTTPHEADER, $this->header);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($curl,CURLOPT_CUSTOMREQUEST,"PUT");
        curl_setopt($curl,CURLOPT_POSTFIELDS, json_encode($this->data));
        curl_setopt($curl,CURLOPT_TIMEOUT, $this->timeout);
        $output = curl_exec($curl);
        curl_close($curl);
        return json_decode($output,true);
    }

    /**
     * @return mixed
     */
    public function delete()
    {
        $curl = curl_init();
        curl_setopt($curl,CURLOPT_URL,$this->url);
        curl_setopt($curl,CURLOPT_HTTPHEADER, $this->header);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($curl,CURLOPT_CUSTOMREQUEST,"DELETE");
        curl_setopt($curl,CURLOPT_POSTFIELDS, json_encode($this->data));
        curl_setopt($curl,CURLOPT_TIMEOUT, $this->timeout);
        $output = curl_exec($curl);
        curl_close($curl);
        return json_decode($output,true);
    }

    /**
     * @return mixed
     */
    public function patch()
    {
        $curl = curl_init();
        curl_setopt($curl,CURLOPT_URL,$this->url);
        curl_setopt($curl,CURLOPT_HTTPHEADER, $this->header);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($curl,CURLOPT_CUSTOMREQUEST,"PATCH");
        curl_setopt($curl,CURLOPT_POSTFIELDS, json_encode($this->data));
        curl_setopt($curl,CURLOPT_TIMEOUT, $this->timeout);
        $output = curl_exec($curl);
        curl_close($curl);
        return json_decode($output,true);
    }
}
