<?php
/**
 *
 * User: cfn
 * Date: 2020/9/30
 * Email: <cfn@leapy.cn>
 */

namespace link\package\office;

/**
 * Class ExcelService
 * @package link\package\office
 */
class ExcelService
{
    /**
     * 单元格宽
     * @var int
     */
    protected $cellWidth = 20;

    /**
     * 单元格搞
     * @var int
     */
    protected $cellHeight = 50;

    /**
     * 单元格表行
     * @var string[]
     */
    protected $cellKey = [
        'A','B','C','D','E','F','G','H','I','G','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
        'AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ',
        'BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ'];

    /**
     * 文件名 && title
     * @var string
     */
    protected $title = "大标题";

    /**
     * 二级表头
     * @var string
     */
    protected $subTitle = "二级标题";

    /**
     * 表头数据
     * @var array
     */
    protected $header = [];

    /**
     * 列数
     * @var int
     */
    protected $colNum = 0;

    /**
     * 表头行数
     * @var int
     */
    protected $headerNum = 3;

    /**
     * Excel 对象
     * @var null
     */
    protected $excel = null;

    /**
     * 样式
     * @var array
     */
    protected $styleArray = array(
        'borders' => array(
            'allborders' => array('style' => \PHPExcel_Style_Border::BORDER_THIN),
        ),
        'font'=>[
            'bold'=>true
        ],
        'alignment'=>[
            'horizontal'=>\PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical'=>\PHPExcel_Style_Alignment::VERTICAL_CENTER
        ]
    );

    /**
     * Excel 初始化
     */
    public function __construct()
    {
        $this->excel = new \PHPExcel();
    }

    /**
     * 设置头部
     * @param string $title
     * @param array $header
     * @param string|null $subTitle
     * @throws \PHPExcel_Exception
     */
    public function setHeader(string $title, array $header, string $subTitle = null)
    {
        $this->header = $header;
        $this->title = $title;
        $this->colNum = count($header);
        if (count($this->cellKey) < count($this->header)) exit(app("packet")->fail("表格列数过长"));
        if (!$subTitle) $this->subTitle = "生成时间：".date('Y-m-d H:i:s',time());
        else $this->subTitle = $subTitle;
        $this->setTitle();
    }

    /**
     * 表头
     * @return $this
     * @throws \PHPExcel_Exception
     */
    protected function setTitle()
    {
        // 默认设置
        $this->excel->getProperties()
            ->setCreator("Neo")
            ->setLastModifiedBy("Neo")
            ->setTitle(iconv('utf-8', 'utf-8', $this->title))
            ->setSubject("Sheet1")
            ->setDescription("")
            ->setKeywords("Sheet1")
            ->setCategory("");
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle("Sheet1");
        // 标题
        $this->excel->getActiveSheet()->setCellValue('A1', $this->title);
        $this->excel->getActiveSheet()->getRowDimension(1)->setRowHeight(50);
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->mergeCells('A1:'.($this->cellKey[$this->colNum-1]).'1');
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setName('黑体');
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(24);
        $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
        // 二级标题
        $this->excel->getActiveSheet()->setCellValue('A2',$this->subTitle);
        $this->excel->getActiveSheet()->getRowDimension(2)->setRowHeight(20);
        $this->excel->getActiveSheet()->mergeCells('A2:'.($this->cellKey[$this->colNum-1]).'2');
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setName('宋体');
        $this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
        // 表头
        $sheet=$this->excel->getActiveSheet();
        foreach($this->header as $key=>$val){
            $row=$this->cellKey[$key];
            $sheet->getColumnDimension($row)->setWidth(isset($val['w'])?$val['w']:$this->cellWidth);
            $sheet->setCellValue($row.$this->headerNum,isset($val['name'])?$val['name']:$val);
            $sheet->getStyle($row)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        $sheet->getStyle('A1:'.($this->cellKey[$this->colNum-1])."1")->applyFromArray($this->styleArray);
        $sheet->getStyle('A2:'.($this->cellKey[$this->colNum-1])."2")->applyFromArray($this->styleArray);
        $sheet->getStyle('A3:'.($this->cellKey[$this->colNum-1])."3")->applyFromArray($this->styleArray);
        $sheet->getDefaultRowDimension()->setRowHeight($this->cellHeight);
        return $this;
    }

    /**
     * 表格内容
     * @param array $data
     * @return void
     * @throws \PHPExcel_Exception
     */
    public function setBody(array $data)
    {
        $sheet = $this->excel->getActiveSheet();
        $cellKey = array_slice($this->cellKey,0,$this->colNum);
        if($data!==null && is_array($data)){
            foreach ($cellKey as $k=>$v){
                foreach ($data as $key=>$val){
                    if(isset($val[$k]) && !is_array($val[$k])){
                        $sheet->setCellValue($v.($this->headerNum+1+$key),$val[$k]);
                    }else if(isset($val[$k]) && is_array($val[$k])){
                        $str='';
                        foreach ($val[$k] as $value){
                            $str.=$value.chr(10);
                        }
                        $sheet->setCellValue($v.($this->headerNum+1+$key),$str);
                    }
                }
            }
        }
    }

    /**
     * 保存文件
     * @throws \PHPExcel_Reader_Exception
     * @throws \PHPExcel_Writer_Exception
     */
    public function save()
    {
        $objWriter=\PHPExcel_IOFactory::createWriter($this->excel,'Excel2007');
        $filename=$this->title.'--'.time().'.xlsx';
        ob_end_clean();
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        exit($objWriter->save('php://output'));
    }

    /**
     * 解析
     * @param string $filePath
     * @return array
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public function parse(string $filePath)
    {
        $xlsReader = \PHPExcel_IOFactory::createReader('Excel2007');
        $xlsReader->setReadDataOnly(true);
        $xlsReader->setLoadSheetsOnly(true);
        $Sheets = $xlsReader->load($filePath);
        $data = $Sheets->getSheet(0);
        return $data ? $data->toArray() : [];
    }
}
