<?php
/**
 * 定时器
 * User: cfn
 * Date: 2020/9/29
 * Email: <cfn@leapy.cn>
 */

namespace link\subscribe;


use link\tools\Http;

/**
 * 定时器
 * Class Timer
 * @package link\subscribe
 */
class Timer
{
    /**
     * 10s
     */
    public function onTimer_10()
    {
        echo "10s定时任务 - ".date("Y-m-d H:i:s")."\n";
    }

    /**
     * 30s
     */
    public function onTimer_30()
    {
        echo "30s定时任务 - ".date("Y-m-d H:i:s")."\n";
    }
}
