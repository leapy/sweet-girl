<?php
/**
 *
 * User: cfn
 * Date: 2020/10/13
 * Email: <cfn@leapy.cn>
 */

namespace link\subscribe;

use Swoole\Server;
use think\swoole\Websocket;

/**
 * Class SocketEvent
 * @package link\subscribe
 */
class SocketEvent
{
    /**
     * @var Websocket|null
     */
    public $websocket = null;

    /**
     * @var Server|null
     */
    public $server = null;

    /**
     * @var object|\think\App|null
     */
    public $users = null;

    /**
     * SocketEvent constructor.
     * @param Server $server
     * @param Websocket $websocket
     */
    public function __construct(Server $server, Websocket $websocket)
    {
        $this->websocket = $websocket;
        $this->server = $server;
        $this->users = app("swoole.table.user");
    }

    /**
     * 公开广播
     * @param array $event
     */
    public function onSendAll(array $event)
    {
        foreach ($this->users as $fd => $user)
        {
            $this->server->push($fd,app("packet")->success("message",$event));
        }
    }

    public function onConnect()
    {
        var_dump("连接成功！");
    }
}
