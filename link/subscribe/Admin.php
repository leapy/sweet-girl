<?php


namespace link\subscribe;

use app\model\admin\AdminLog;

/**
 * 事件监听
 * Class Admin
 * @package app\admin\subscribe
 */
class Admin
{
    /**
     * 用户登录前置操作
     * @param $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function onAdminLoginBefore($data)
    {
        $adminInfo = \app\model\admin\Admin::getAdminInfo($data['id']);
        $adminInfo->last_ip = request()->ip();
        $adminInfo->last_time = date("Y-m-d H:i:s");
        $adminInfo->count++;
        $adminInfo->save();
    }

    /**
     * @param $data
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function onAdminTLog($data)
    {
        list($adminInfo,$module,$controller,$action) = $data;
        AdminLog::saveLog($adminInfo,$module,$controller,$action);
    }
}
