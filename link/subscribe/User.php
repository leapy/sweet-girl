<?php
/**
 *
 * User: cfn
 * Date: 2020/11/7
 * Email: <cfn@leapy.cn>
 */

namespace link\subscribe;

use app\model\user\UserBill as bModel;
use app\model\user\User as uModel;
use link\tools\Log;
use think\facade\Db;

/**
 * Class User
 * @package link\subscribe
 */
class User
{
    /**
     * 注册
     * @param array $data
     */
    public function onUserRegister(array $data)
    {
        // 注册成功，奖励积分
        Db::startTrans();
        try {
            $uid = $data['uid'];
            // 账单表
            bModel::addBill([
                'uid' => $uid,
                'title' => "注册成功，奖励积分",
                'bill_type' => 1,
                'amount' => systemConfig("register_ok_integral"),
                'change_type' => "register_ok_integral",
                'result' => bcadd(uModel::where("uid",$uid)->value("integral"),systemConfig("register_ok_integral"),0),
                'remark' => ''
            ]);
            uModel::addIntegral($uid,systemConfig("register_ok_integral"));
            // 如果有上级推荐人 给奖励
            Db::commit();
        }catch (\Exception $e)
        {
            Log::error("userRegister",$e->getMessage());
            Db::rollBack();
        }
    }
}
