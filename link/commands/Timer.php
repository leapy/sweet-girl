<?php


namespace link\commands;


use link\tools\Log;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;

/**
 * 定时器
 * Class Timer
 * @package link\commands
 */
class Timer extends Command
{
    /**
     * 间隔多少毫秒执行
     * @var int
     */
    protected $ms = 1000;

    /**
     * 定时器编号
     * @var null
     */
    protected $timer_id = null;

    /**
     * 服务
     * @var null
     */
    protected $server = null;

    /**
     * 配置
     */
    protected function configure()
    {
        $this->setName('timer')
            ->addArgument('name', Argument::OPTIONAL, 'start|stop',"start")
            ->addOption('daemon', "d", Option::VALUE_NONE, '守护进程')
            ->setDescription('Swoole Timer');
    }

    /**
     * @param Input $input
     * @param Output $output
     * @return int|void|null
     */
    protected function execute(Input $input, Output $output)
    {
        $name = trim($input->getArgument('name'));
        // 判断命令是否存在
        if (!in_array($name, ['start','stop'])) exit("no such option <$name> \n");
        // 执行命令
        $this->{$name}();
    }

    /**
     * 启动
     */
    public function start()
    {
        $this->server = new \swoole_server("0.0.0.0","9501");
        $this->server->set(config("timer"));
        $this->server->on("start",function () {
            $now = time();
            $interval = [10 => $now, 30 => $now];
            $this->timer_id = \Swoole\Timer::tick($this->ms, function () use (&$interval){
                try {
                    $now = time();
                    foreach ($interval as $sec => $time) {
                        if ($now - $time >= $sec) {
                            event('Timer_' . $sec);
                            $interval[$sec] = $now;
                        }
                    }
                } catch (\Throwable $e) {
                    Log::error("timer", $e->getMessage());
                }
            });
        });
        $this->server->on("receive",function () {});
        $this->server->on("task",function () {});
        $this->server->start();
    }

    /**
     * 定时器关闭
     */
    public function stop()
    {
        \Swoole\Timer::clear($this->timer_id);
    }
}
