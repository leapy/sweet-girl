<?php


namespace link\swoole;


use Swoole\Server;
use Swoole\Table;
use Swoole\Websocket\Frame;
use Swoole\WebSocket\Server as WebsocketServer;
use think\Config;
use think\Request;
use think\swoole\contract\websocket\HandlerInterface;

/**
 * Class Handler
 * @package link\swoole
 */
class Handler implements HandlerInterface
{
    /** @var WebsocketServer */
    public $server;

    /** @var Config */
    protected $config;

    /**
     * @var Events
     */
    private $event;

    /**
     * 注册方法 未注册不能使用
     * @var string[]
     */
    public $method = ['login','ping','close','message'];

    /**
     * @var object|\think\App
     */
    public $users;

    /**
     * Handler constructor.
     * @param Server $server
     * @param Config $config
     */
    public function __construct(Server $server, Config $config)
    {
        $this->server = $server;
        $this->config = $config;
        $this->users = app("swoole.table.user");
        $this->event = new Events($this);
    }

    /**
     * "onOpen" listener.
     *
     * @param int     $fd
     * @param Request $request
     */
    public function onOpen($fd, Request $request)
    {
        if (!$request->param('sid')) {
            $payload        = json_encode(
                [
                    'sid'          => base64_encode(uniqid()),
                    'upgrades'     => [],
                    'pingInterval' => $this->config->get('swoole.websocket.ping_interval'),
                    'pingTimeout'  => $this->config->get('swoole.websocket.ping_timeout'),
                ]
            );
            $this->server->push($fd, $payload);
        }
    }

    /**
     * "onMessage" listener.
     *  only triggered when event handler not found
     *
     * @param Frame $frame
     * @return bool
     */
    public function onMessage(Frame $frame)
    {
        $packet = $frame->data;

        // 解析数据
        list($event, $data) = app("packet")->decode($packet);

        // 判断数据格式是否有误
        if (is_null($event))
            return $this->server->push($frame->fd, app("packet")->error("数据格式错误"));

        // 如果类中方法存在就调用
        if (in_array($event, $this->method)) $this->event->{$event}($frame->fd, $data);

        return true;
    }

    /**
     * "onClose" listener.
     *
     * @param int $fd
     * @param int $reactorId
     */
    public function onClose($fd, $reactorId)
    {
        $this->unJoin($fd);
    }

    /**
     * 心跳
     * @param $fd
     */
    public function checkHeartbeat($fd)
    {
        $this->server->push($fd, json_encode(['event'=>'pong']));
    }

    /**
     * 加入
     * @param $fd
     * @param $adminInfo
     */
    public function join($fd, $adminInfo)
    {
        $this->users->set((string)$fd, ['id' => $adminInfo['id'],'uid'=>$adminInfo['uid'],'fd'=>$fd,'role'=>$adminInfo['role_id']]);
    }

    /**
     * 退出
     * @param $fd
     */
    public function unJoin($fd)
    {
        $this->users->del((string)$fd);
    }

    /**
     * 退出 踢出
     * @param $fd
     */
    public function close($fd)
    {
        $this->unJoin($fd);
        $this->server->close($fd, app("packet")->error("已断开"));
    }
}
