<?php
/**
 *
 * User: cfn
 * Date: 2020/10/10
 * Email: <cfn@leapy.cn>
 */

namespace link\swoole;

/**
 * Class Client
 * @package link\swoole
 */
class Client
{
    /**
     * @var \Swoole\Client
     */
    public $client;

    /**
     * Client constructor.
     */
    public function __construct()
    {
        $this->client = new \Swoole\Client(SWOOLE_SOCK_TCP);
    }

    /**
     * send.
     * @param $data
     */
    public function send($data)
    {
        if ($this->client->connect("127.0.0.1",config("swoole.server.port")))
            $this->client->send($data);
        $this->client->close();
    }
}
