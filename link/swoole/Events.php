<?php


namespace link\swoole;


use link\tools\Jwt;
use link\tools\swoole\Redis;

/**
 * Class BackEvents
 * @package link\swoole
 */
class Events
{
    /**
     * @var Handler
     */
    public $handler;

    /**
     * Event constructor.
     * @param Handler $handler
     */
    public function __construct(Handler $handler)
    {
        $this->handler = $handler;
    }

    /**
     * 登录
     * @param $fd
     * @param array $data
     * @return mixed
     */
    public function login($fd, $data = [])
    {
        try {
            $adminInfo = Jwt::parserToken($data['token']);
            if (!is_null($adminInfo) && is_array($adminInfo)) {
                // 验证账号是否在别处登录
                $token = Redis::connect()->get(md5($adminInfo['id']));
                if (empty($token)) return $this->handler->server->push($fd, app("packet")->warning("登录已过期"));
                if (unserialize($token) != $data['token']) return $this->handler->server->push($fd, app("packet")->warning("已在别处登录！"));
                $this->handler->join($fd, $adminInfo);
                return $this->handler->server->push($fd, app("packet")->success("message", "登录成功"));
                // 登录成功
            }else return $this->handler->server->push($fd, app("packet")->fail("token签发有误！"));
        }catch (\Exception $e)
        {
            return $this->handler->server->push($fd, app("packet")->fail($e->getMessage()));
        }
    }

    /**
     * ping.
     * @param $fd
     * @param array $data
     * @return mixed
     */
    public function message($fd, $data = [])
    {
        return $this->handler->server->push($fd, app("packet")->success("message", $data));
    }

    /**
     * ping.
     * @param $fd
     * @param array $data
     */
    public function ping($fd, $data = [])
    {
        $this->handler->checkHeartbeat($fd);
    }

    /**
     * 断开连接
     * @param $fd
     * @param array $data
     */
    public function close($fd, $data = [])
    {
        $this->handler->close($fd);
    }
}
