<?php
/**
 *
 * User: cfn
 * Date: 2020/9/29
 * Email: <cfn@leapy.cn>
 */

namespace link\tools;

/**
 * 日志记录重新设置
 * Class Log
 * @package link\tools
 */
class Log
{
    /**
     * @param string $tag 标签
     * @param null $msg 数据
     */
    public static function debug(string $tag, $msg = null)
    {
        \think\facade\Log::debug($tag."：{msg}",['msg'=>is_string($msg) ?: json_encode($msg)]);
    }

    /**
     * @param string $tag 标签
     * @param null $msg 数据
     */
    public static function info(string $tag, $msg = null)
    {
        \think\facade\Log::info($tag."：{msg}",['msg'=>is_string($msg) ?: json_encode($msg)]);
    }

    /**
     * @param string $tag 标签
     * @param null $msg 数据
     */
    public static function notice(string $tag, $msg = null)
    {
        \think\facade\Log::notice($tag."：{msg}",['msg'=>is_string($msg) ?: json_encode($msg)]);
    }

    /**
     * @param string $tag 标签
     * @param null $msg 数据
     */
    public static function warning(string $tag, $msg = null)
    {
        \think\facade\Log::warning($tag."：{msg}",['msg'=>is_string($msg) ?: json_encode($msg)]);
    }

    /**
     * @param string $tag 标签
     * @param null $msg 数据
     */
    public static function error(string $tag, $msg = null)
    {
        \think\facade\Log::error($tag."：{msg}",['msg'=>is_string($msg) ?: json_encode($msg)]);
    }

    /**
     * @param string $tag 标签
     * @param null $msg 数据
     */
    public static function critical(string $tag, $msg = null)
    {
        \think\facade\Log::critical($tag."：{msg}",['msg'=>is_string($msg) ?: json_encode($msg)]);
    }

    /**
     * @param string $tag 标签
     * @param null $msg 数据
     */
    public static function alert(string $tag, $msg = null)
    {
        \think\facade\Log::alert($tag."：{msg}",['msg'=>is_string($msg) ?: json_encode($msg)]);
    }

    /**
     * @param string $tag 标签
     * @param null $msg 数据
     */
    public static function emergency(string $tag, $msg = null)
    {
        \think\facade\Log::emergency($tag."：{msg}",['msg'=>is_string($msg) ?: json_encode($msg)]);
    }
}
