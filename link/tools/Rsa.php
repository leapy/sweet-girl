<?php
/**
 * rsa算法
 * User: cfn
 * Date: 2020/10/12
 * Email: <cfn@leapy.cn>
 */

namespace link\tools;

/**
 * Class Rsa
 * @package link\tools
 */
class Rsa
{
    /**
     * 私钥
     * @var null
     */
    private $privateKey = null;

    /**
     * 公钥
     * @var null
     */
    private $publicKey = null;

    /**
     * Rsa constructor.
     */
    public function __construct()
    {

    }

    /**
     * 创建密钥对，公钥和私钥
     */
    public function keyPair()
    {
        $config = [
            "config" => "/www/server/php/72/src/ext/openssl/tests/openssl.cnf",
            "digest_alg" => "sha512",
            "private_key_bits" => 4096,
            "private_key_type" => OPENSSL_KEYTYPE_RSA,
        ];
        $rsa = openssl_pkey_new($config);
        openssl_pkey_export($rsa, $key, NULL, $config);
        // 私钥
        $this->privateKey = openssl_pkey_get_public($key);
        var_dump($this->privateKey);
        $rsaPri = openssl_pkey_get_details($rsa);
        // 生成公钥
        $this->publicKey = openssl_pkey_get_public($rsaPri['key']);
        var_dump($this->publicKey);
    }
}
