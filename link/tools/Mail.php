<?php


namespace link\tools;

use link\package\email\Ym163MailService;

/**
 * 邮件助手
 * Class Mail
 * @package link\tools
 */
class Mail
{
    /**
     * 登录
     * @param string $to
     * @param $code
     * @return false
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function loginCode(string $to, $code)
    {
        return self::send($to, "登录验证", self::buildText("mail_login",$code), "html");
    }

    /**
     * 修改邮箱
     * @param string $to
     * @param $code
     * @return false
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function modifyCode(string $to, $code)
    {
        return self::send($to, "修改邮箱", self::buildText("mail_modify", $code), "html");
    }

    /**
     * 注册账号
     * @param string $to
     * @param $code
     * @return false
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function register(string $to, $code)
    {
        return self::send($to, "注册账号", self::buildText("mail_register", $code), "html");
    }

    /**
     * 找回密码
     * @param string $to
     * @param $code
     * @return false
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function retrieve(string $to, $code)
    {
        return self::send($to, "找回密码", self::buildText("mail_retrieve", $code), "html");
    }

    /**
     *
     * @param string $type
     * @param $code
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function buildText(string $type="mail_login", $code="123456")
    {
        // 获取登录模板
        $text = systemConfig($type);
        // 转换成html实体
        $text = htmlspecialchars_decode($text);
        // 替换占位
        $text = sprintf($text, $code);
        return $text;
    }

    /**
     * 发送
     * @param string $to       // 发给谁
     * @param $subject         // 主题
     * @param $body            // 内容
     * @param string $subtype  // 邮件类型
     * @return false
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function send(string $to, $subject, $body, $subtype="plain")
    {
        switch (systemConfig("mail_type"))
        {
            case 1:
                $mail = new Ym163MailService(systemConfig(['mail_host','mail_port','mail_password','mail_username','mail_from_name','mail_type']));
                $mail->to($to);
                $mail->subtype($subtype);
                return $mail->send($subject, $body);
            default:
                return false;
        }
    }
}
