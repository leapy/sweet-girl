<?php
namespace link\tools;

use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT as jt;
use Firebase\JWT\SignatureInvalidException;

/**
 * token 生成解析类
 * Class Jwt
 * @package link\tools
 */
class Jwt
{
    /**
     * 生成 toekn
     * @param array $param
     * @return string
     */
    public static function createToken(array $param): string
    {
        $salt = config('token.salt');
        $expire = config('token.expire');
        $token = array(
            "iss"=> $salt,
            "aud"=> $salt,
            "iat"=> time(),
            "nbf"=> time(),
            "exp"=> time()+$expire,
            "data"=> $param
        );
        return "Bearer ".jt::encode($token, $salt, "HS256");
    }

    /**
     * 解析token
     * @param string $token
     * @return array
     */
    public static function parserToken(string $token): array
    {
        try {
            $data = jt::decode(trim(ltrim($token, 'Bearer')), config('token.salt'), ["HS256"]);
            $data = (array)$data;
            return (array)$data['data'];
        }catch(SignatureInvalidException $e) {
            $status['msg']=app()->request->domain();
            return $status;
        }catch(BeforeValidException $e) {
            $status['msg']="token失效";
            return $status;
        }catch(ExpiredException $e) {
            $status['msg']="token失效";
            return $status;
        }catch(\Exception $e) {
            $status['msg'] = "未知错误";
            return $status;
        }
    }
}
