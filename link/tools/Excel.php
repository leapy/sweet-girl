<?php
/**
 *
 * User: cfn
 * Date: 2020/10/9
 * Email: <cfn@leapy.cn>
 */

namespace link\tools;

use link\package\office\ExcelService;

/**
 * Class Excel
 * @package link\tools
 */
class Excel
{
    /**
     * 导出
     * @param string $title
     * @param array $header
     * @param array $data
     * @param string|null $subTitle
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     * @throws \PHPExcel_Writer_Exception
     */
    public static function export(string $title, array $header, array $data, string $subTitle = null)
    {
        $excel = new ExcelService();
        $excel->setHeader($title, $header, $subTitle);
        $excel->setBody($data);
        $excel->save();
    }

    /**
     * 导入获取行数据
     * @param string $filePath
     * @return array
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public static function import(string $filePath)
    {
        $excel = new ExcelService();
        return $excel->parse($filePath);
    }
}
