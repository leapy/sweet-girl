<?php


namespace link\tools;

use app\Request;

/**
 * 参数处理
 * Class param
 * @package link\tools
 */
class Argv
{
    /**
     * 解析post参数
     * @param array $params
     * @param $request
     * @return array
     */
    public static function post(array $params, $request = null)
    {
        if (is_null($request)) $request = app("request");
        $data = [];
        foreach ($params as $param){
            if(!is_array($param)) {
                $data[$param] = $request->param($param);
            }else{
                $data[$param[0]] = $request->param($param[0], isset($param[1]) ? $param[1] : '');
            }
        }
        return $data;
    }

    /**
     * 解析get参数
     * @param array $params
     * @param $request
     * @return array
     */
    public static function get(array $params, $request = null)
    {
        if (is_null($request)) $request = app("request");
        $data = [];
        foreach ($params as $param){
            if(!is_array($param)) {
                $data[$param] = $request->param($param);
            }else{
                $data[$param[0]] = $request->param($param[0], isset($param[1]) ? $param[1] : '');
            }
        }
        return $data;
    }
}
