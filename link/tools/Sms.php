<?php


namespace link\tools;

use link\package\sms\QCloudSmsService;

/**
 * 短信助手
 * Class Sms
 * @package link\tools
 */
class Sms
{
    /**
     * 登录
     * @param $mobile
     * @param $code
     * @return false
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function loginCode($mobile, $code)
    {
        $sms_login = systemConfig("sms_login");
        return self::send($sms_login, $mobile, [$code]);
    }

    /**
     * 修改
     * @param $mobile
     * @param $code
     * @return false
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function modifyCode($mobile, $code)
    {
        $sms_login = systemConfig("sms_modify");
        return self::send($sms_login, $mobile, [$code]);
    }

    /**
     * 注册
     * @param $mobile
     * @param $code
     * @return false
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function register($mobile, $code)
    {
        $sms_login = systemConfig("sms_register");
        return self::send($sms_login, $mobile, [$code,5]);
    }

    /**
     * 找回密码
     * @param $mobile
     * @param $code
     * @return false
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function retrieve($mobile, $code)
    {
        $sms_login = systemConfig("sms_retrieve");
        return self::send($sms_login, $mobile, [$code]);
    }
    /**
     * 发送
     * @param $templId
     * @param $mobile
     * @param $params
     * @return false
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function send($templId, $mobile, $params)
    {
        switch (systemConfig("sms_type"))
        {
            case 1:
                return QCloudSmsService::app()->setPhoneNumbers($mobile)->sendSingleSms($templId, $params, systemConfig("sms_sign"));
            default:
                return false;
        }
    }
}
