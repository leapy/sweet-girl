<?php
/**
 *
 * User: cfn
 * Date: 2020/9/29
 * Email: <cfn@leapy.cn>
 */

namespace link\tools\swoole;


class Redis
{
    /**
     * @return \Swoole\Coroutine\Redis
     */
    public static function connect()
    {
        $redis = new \Swoole\Coroutine\Redis();
        $redis->connect(config("cache.stores.redis.host"), config("cache.stores.redis.port"));
        $redis->auth(config("cache.stores.redis.password"));
        $redis->select(config("cache.stores.redis.select"));
        return $redis;
    }
}
