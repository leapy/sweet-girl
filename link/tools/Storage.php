<?php


namespace link\tools;

use link\package\storage\QCloudCosService;
use think\facade\Filesystem;

/**
 * 存储
 * Class Storage
 * @package link\tools
 */
class Storage
{
    /**
     * 上传
     * @param $file
     * @return false|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function put($file)
    {
        switch (systemConfig("storage_type"))
        {
            case 2:
                $name = '/image/'.date('Ymd')."/".substr(md5($file->getRealPath()) , 0, 5). date('YmdHis') . rand(0, 9999) . '.' . $file->getOriginalExtension();
                return (new QCloudCosService())->put($name, $file->getRealPath());
            default:
                return systemConfig("asset_domain") . "/upload/".Filesystem::disk('public')->putFile( 'image', $file);
        }
    }

    /**
     * 删除资源
     * @param string $fileName
     * @param int $storage_type
     * @return bool|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function del(string $fileName, int $storage_type)
    {
        switch ($storage_type)
        {
            case 2:
                return (new QCloudCosService())->del($fileName);
            default:
                return unlink(app()->getRootPath() . "public" . str_replace(systemConfig("asset_domain"),"", $fileName));
        }
    }
}
