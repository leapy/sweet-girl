<?php
/**
 *
 * User: cfn
 * Date: 2020/10/14
 * Email: <cfn@leapy.cn>
 */

namespace link\tools;


use link\package\http\Curl;

class Http
{
    /**
     * @param string $url
     * @param array $header
     * @param int $timeout
     * @return mixed
     */
    public static function get(string $url, array $header = [], int $timeout = 30)
    {
        $curl = new Curl($url,[],$header,$timeout);
        return $curl->get();
    }

    /**
     * @param string $url
     * @param array $data
     * @param array $header
     * @param int $timeout
     * @return mixed
     */
    public static function post(string $url, array $data, array $header = [], int $timeout = 30)
    {
        $curl = new Curl($url,$data,$header,$timeout);
        return $curl->post();
    }
}
