<?php


namespace link\tools;

use think\Response;

/**
 * json返回工具类
 * Class Json
 * @package link\tools
 */
class Json
{
    /**
     * 成功返回200
     * @var int
     */
    public static $SUCCESS_CODE = 200;

    /**
     * 失败返回400
     * @var int
     */
    public static $FAIL_CODE = 400;

    /**
     * 警告返回401
     * @var int
     */
    public static $WARNING_CODE = 401;

    /**
     * 失错误返回403
     * @var int
     */
    public static $ERROR_CODE = 403;

    /**
     * 成功返回信息
     * @var string
     */
    private static $SUCCESS_MSG = "success";

    /**
     * 失败返回信息
     * @var string
     */
    private static $FAIL_MSG = "fail";

    /**
     * 失败返回信息
     * @var string
     */
    private static $WARNING_MSG = "warning";

    /**
     * 失败返回信息
     * @var string
     */
    private static $ERROR_MSG = "error";

    /**
     * 成功返回数据
     * @param string|array|null $msg
     * @param array|null $data
     * @return Response
     */
    public function success($msg = null, ?array $data = null)
    {
        $code = self::$SUCCESS_CODE;
        $res = compact('code','msg', 'data');
        if (is_null($msg)) $res['msg'] = self::$SUCCESS_MSG;
        if (is_array($msg))
        {
            $res['data'] = $msg;
            $res['msg'] = self::$SUCCESS_MSG;
        }
        if (empty($res['data'])) unset($res['data']);
        return Response::create($res, 'json', 200);
    }

    /**
     * 失败返回数据
     * @param string|null $msg
     * @param array|null $data
     * @return Response
     */
    public function fail(?string $msg = null, ?array $data = null)
    {
        $code = self::$FAIL_CODE;
        $res = compact('code','msg', 'data');
        if (is_null($msg))
        {
            $res['msg'] = self::$FAIL_MSG;
        }else
        {
            if (is_array($msg))
            {
                $res['data'] = $msg;
                $res['msg'] = self::$FAIL_MSG;
            }else
            {
                $res['msg'] = $msg;
            }
        }
        if (empty($res['data'])) unset($res['data']);
        return Response::create($res, 'json', 200);
    }

    /**
     * 失败返回数据
     * @param string|null $msg
     * @param array|null $data
     * @return Response
     */
    public function warning(?string $msg = null, ?array $data = null)
    {
        $code = self::$WARNING_CODE;
        $res = compact('code','msg', 'data');
        if (is_null($msg))
        {
            $res['msg'] = self::$FAIL_MSG;
        }else
        {
            if (is_array($msg))
            {
                $res['data'] = $msg;
                $res['msg'] = self::$WARNING_MSG;
            }else
            {
                $res['msg'] = $msg;
            }
        }
        if (empty($res['data'])) unset($res['data']);
        return Response::create($res, 'json', 200);
    }

    /**
     * 失败返回数据
     * @param string|null $msg
     * @param array|null $data
     * @return Response
     */
    public function error(?string $msg = null, ?array $data = null)
    {
        $code = self::$ERROR_CODE;
        $res = compact('code','msg', 'data');
        if (is_null($msg))
        {
            $res['msg'] = self::$ERROR_MSG;
        }else
        {
            if (is_array($msg))
            {
                $res['data'] = $msg;
                $res['msg'] = self::$FAIL_MSG;
            }else
            {
                $res['msg'] = $msg;
            }
        }
        if (empty($res['data'])) unset($res['data']);
        return Response::create($res, 'json', 200);
    }
}
