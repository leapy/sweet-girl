<?php


namespace link\exception;

/**
 * Class StorageException
 * @package link\exception
 */
class StorageException extends \RuntimeException
{
    /**
     * 错误信息
     * @var string
     */
    protected $message;

    /**
     * ModelException constructor.
     * @param string $message
     */
    public function __construct($message = "未知错误")
    {
        if (is_array($message)) $message = json_encode($message, true);
        parent::__construct($message);
    }

    /**
     * 获取错误信息
     * @access public
     * @return array|string
     */
    public function getErrorMessage()
    {
        return $this->message;
    }
}
