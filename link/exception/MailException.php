<?php

declare (strict_types = 1);

namespace link\exception;

/**
 * 发送邮件异常
 * Class MailException
 * @package link\exception
 */
class MailException extends \RuntimeException
{
    /**
     * 错误信息
     * @var string
     */
    protected $message;

    /**
     * ModelException constructor.
     * @param string $message
     */
    public function __construct($message = "未知错误")
    {
        if (is_array($message)) $message = json_encode($message, true);
        parent::__construct($message);
    }

    /**
     * 获取错误信息
     * @access public
     * @return array|string
     */
    public function getErrorMessage()
    {
        return $this->message;
    }
}
